function phase_mask = load_adc_phase_file(filename, ncols)
%Load ADC phase file for reconstruction
%   Detailed explanation goes here



f = load(filename);
phase_vec = f.adc_phases;

% expand the phase offsets along the readout axis
phase_mask = repmat(phase_vec, 1, ncols);

phase_mask = exp(-1i*phase_mask);


end

