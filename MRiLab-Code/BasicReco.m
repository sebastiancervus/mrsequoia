function BasicReco
%Create an external reconstruction based on your code  
global VCtl      % use VCtl structure, read only  
global VSig      % use VSig structure, read only  
global VImg      % use VImg structure, read and write  
 
% The main code for your reconstruction  
S = VSig.Sx + 1i*VSig.Sy;

%n_ro = VCtl.ResFreq-1;  % why is VCtl.ResFreq by 1 larger than the value in the GUI???

% For some reasons, VCtl.ResFreq is sometimes correct and sometimes 1 more
% than specified. Let's find out which one is the case here:
if numel(S) < VCtl.ResFreq * VCtl.ResPhase -1  % -1 is the missing data point, see below
    n_ro = VCtl.ResFreq - 1;
else
    n_ro = VCtl.ResFreq;
end

if mod(numel(S), n_ro) ~= 0
   %S = [S 0];  % append missing data point to the END of the signal vector 
   S = [S conj(S(1))];
end

figure;

plt_rows = 2;
plt_cols = 3;
subplot(plt_rows, plt_cols, 1)
plot(abs(S))
title('Signal');

n_pe = numel(S) / n_ro;
ksp = reshape(S, n_ro, n_pe).';

if ~ischar(VCtl.CV1) || strcmp(VCtl.CV1, '')  % no phase offset file specified
    phase_offsets = ones(n_pe, n_ro);
else
    phase_offsets = load_adc_phase_file(VCtl.CV1, n_ro);
end

ksp = ksp .* phase_offsets;

% Fix the orientation of k-space, depending on the image orientation 

if strcmp(VCtl.FreqDir, 'A/P')
    ksp = rot90(ksp, -1);
end


subplot(plt_rows, plt_cols, 2);
plot(VSig.Kx);
title('Kx');
subplot(plt_rows, plt_cols, 3);
plot(VSig.Ky);
title('Ky');

subplot(plt_rows, plt_cols, 4);
imagesc(abs(ksp));
colormap gray
axis equal
title('k-space');

subplot(plt_rows, plt_cols, 5);
imagesc(angle(phase_offsets));
colormap gray;
colorbar;
axis equal;
title('ADC phase offsets');

subplot(plt_rows, plt_cols, 6);
img = fftshift(ifft2(ksp));

imagesc(abs(img))
colormap gray
axis equal
title('Image');



% Store recon image into VImg structure  
VImg.Real = real(img);  
VImg.Imag = imag(img);  
VImg.Mag = abs(img);  
VImg.Phase = angle(img);  
VImg.Sx = real(ksp);
VImg.Sy = imag(ksp);  
 
end
