from abc import ABC, abstractmethod
from typing import Optional
from typing import Tuple, TypeVar

from AtomList import AtomList
from WaveformManager import WaveformManager
from SimulationProtocol import SimulationProtocol
from Encoder import Encoder

# forward declaration of the SequenceTiminng class (importing would create a cyclic dependency)
SeqTim = TypeVar('SeqTim', bound='SequenceTiming')


class Importer(ABC):
    """
    An abstract base class for importers for different vendors.
    """

    filename: str
    timing: SeqTim
    gx: Optional[Encoder]
    gy: Optional[Encoder]
    gz: Optional[Encoder]
    rf_pha0: Optional[Encoder]
    rf_pha1: Optional[Encoder]
    rf_ch0: Optional[Encoder]
    rf_ch1: Optional[Encoder]
    adc: Optional[Encoder]

    def __init__(self, filename: str, timing: SeqTim):
        self.filename = filename
        self.timing = timing
        self.gx = None
        self.gy = None
        self.gz = None
        self.rf_pha0 = None
        self.rf_pha1 = None
        self.rf_ch0 = None
        self.rf_ch1 = None
        self.adc = None

    @abstractmethod
    def get_atoms(self) -> Tuple[AtomList, AtomList, AtomList, AtomList, AtomList, AtomList, AtomList, AtomList]:
        pass

    @abstractmethod
    def get_protocol(self) -> SimulationProtocol:
        pass
