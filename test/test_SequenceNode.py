"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import unittest
from unittest import mock

from SeqTree.SequenceNode import SequenceNode
from SeqTree.SequenceMediator import SequenceMediator


class SequenceNodeTest(unittest.TestCase):

    @mock.patch.object(SequenceNode, '__abstractmethods__', set())
    def setUp(self):
        self.med = mock.create_autospec(SequenceMediator)
        self.root = SequenceNode('Root', self.med)
        self.child1 = SequenceNode('Child 1', self.med)
        self.child2 = SequenceNode('Child 2', self.med)
        self.root.add_child(self.child1, self.child2)

    # Since we cannot directly instantiate abstract base class SequenceNode, we have to mock all its abstract methods.
    # See https://stackoverflow.com/questions/9757299/python-testing-an-abstract-base-class
    @mock.patch.object(SequenceNode, '__abstractmethods__', set())
    def test_add_child(self):

        # test that the tree was correctly constructed in the setUp method:
        self.assertTrue(self.root.children[0] is self.child1 and self.root.children[1] is self.child2, 'Setting up the tree in setUp failed.')

        # build an ad-hoc tree and check that it also works when adding the children one after the other
        tmp_root = SequenceNode('Root', None)
        tmp_child1 = SequenceNode('Child 1', None)
        tmp_child2 = SequenceNode('Child 2', None)
        tmp_root.add_child(tmp_child1)

        self.assertTrue(tmp_root.children[0] is tmp_child1)
        tmp_root.add_child(tmp_child2)
        self.assertTrue(tmp_root.children[0] is tmp_child1 and tmp_root.children[1] is tmp_child2)

    @mock.patch.object(SequenceNode, '__abstractmethods__', set())
    def test_get_duration_of_children(self):
        duration_child = 10

        # root.get_duration_of_children calls duration on its chilcren, which is implemented as a property. Therefore,
        # we need to use a PropertyMock on duration in order to test it.

        with mock.patch('SeqTree.SequenceNode.SequenceNode.duration', new_callable=mock.PropertyMock) as patched_duration:
            patched_duration.return_value = duration_child

            duration = self.root.get_duration_of_children()
            self.assertTrue(duration == 2*duration_child)
            self.assertEqual(patched_duration.call_count, 2)

    def test_run_children(self):
        with mock.patch('SeqTree.SequenceNode.SequenceNode.execute') as patched_execute:
            with mock.patch('SeqTree.SequenceNode.SequenceNode.duration',
                            new_callable=mock.PropertyMock) as patched_duration:
                duration_child = 10
                patched_duration.return_value = duration_child

                self.root.run_children()

                self.assertEqual(patched_execute.call_count, 2)
                self.assertEqual(self.med.increment_time.call_count, 2)


                # ensure that SequenceMediator.increment_time was called twice, each time with duration_child as the argument
                for c in self.med.increment_time.call_args_list:
                    args, kwargs = c
                    self.assertEqual(args[0], duration_child)

    def test_delete_child_by_index(self):

        self.root.delete_child_by_index(0)
        self.assertTrue(self.root.children[0] is self.child2)



if __name__ == '__main__':
    unittest.main()



