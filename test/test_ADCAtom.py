"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import unittest
from Atoms.ADCAtom import ADCAtom


class ADCAtomTest(unittest.TestCase):

    def __init__(self, *args):
        super().__init__(*args)
        self.start_time = 3000
        self.name = "ADC-Atom_test"
        self.duration = 1024.0
        self.num_adcs = 512
        self.aa = ADCAtom(self.start_time, self.name, self.duration, self.num_adcs, None)

    def test_constructor(self):

        self.assertTrue(self.aa.start_time == self.start_time)
        self.assertTrue(self.aa.name == self.name)
        self.assertTrue(self.aa.duration == self.duration)
        self.assertTrue(self.aa.num_adcs == self.num_adcs)
        self.assertTrue(self.aa.adc_info is None)
        self.assertTrue(self.aa.offset_phase == 0.0)
        self.assertTrue(self.aa.dynamic_phase == 0)

        self.assertFalse(self.aa.has_adc_info())

    def test_to_mrilab_xml(self):

        xml = self.aa.to_mrilab_xml()
        self.assertTrue(f'Notes="{self.name}"' in xml)
        self.assertTrue(f'tStart="{(self.start_time*1e-6):.9f}"' in xml)
        self.assertTrue(f'tEnd="{(self.start_time+self.duration)*1e-6:.9f}"' in xml)

    def test_str(self):
        s = self.aa.__str__()
        self.assertTrue(f'name {self.name},' in s)
        self.assertTrue(f'start_time {self.start_time},' in s)
        self.assertTrue(f'duration {self.duration:.1f},' in s)
        self.assertTrue(f'num_adc {self.num_adcs}' in s)
