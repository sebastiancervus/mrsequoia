"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import unittest
from Atoms.AtomBase import AtomBase
import unittest.mock as mock
from SeqTree.ActionNode import ActionNode
from SeqTree.SequenceMediator import SequenceMediator
from SeqTree.LoopNode import LoopNode


class ActionNodeTest(unittest.TestCase):

    def setUp(self) -> None:

        self.atom = mock.create_autospec(AtomBase)
        self.med = mock.create_autospec(SequenceMediator)
        self.action_node = ActionNode('ActionNode', self.med, self.atom)

    def test_duration(self):
        """
        Check that the ActionNode returns the correct duration of its Atom.
        :return:
        """

        duration = 100
        self.atom.duration = duration

        self.assertEqual(self.action_node.duration, duration)

    def test_set_call_counter_name(self):

        counter_name = 'MyCounter'
        self.action_node.set_call_counter_name(counter_name)

        self.assertEqual(self.med.set_or_update_shared_variable.call_count, 1)
        self.assertEqual(self.med.set_or_update_shared_variable.call_args_list[0][0][0], counter_name)

    def test_rename_loop_indices(self):

        idx_id1 = 'LoopIdx1'
        idx_id2 = 'LoopIdx2'
        loop_node1 = LoopNode('Loop1', self.med, idx_id1)
        loop_node2 = LoopNode('Loop2', self.med, idx_id2)

        self.action_node.add_child(loop_node1, loop_node2)

        self.assertEqual(self.action_node.children[0].loop_idx_str, idx_id1)
        self.assertEqual(self.action_node.children[1].loop_idx_str, idx_id2)

        prefix = 'new'

        self.action_node.rename_loop_indices(prefix)

        # The index of loop1 should have been renamed; loop2 should still have the original index name
        self.assertEqual(self.action_node.children[0].loop_idx_str, prefix + '_' + idx_id1)
        self.assertEqual(self.action_node.children[1].loop_idx_str, prefix + '_' + idx_id2)


if __name__ == '__main__':
    unittest.main()