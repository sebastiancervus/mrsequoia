"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import unittest
import numpy as np
from LineEncoder import LineEncoder


class LineEncoderTest(unittest.TestCase):

    def test_from_waveform(self):
        dt_us = 10
        waveform = [0, 1, 2, 3, 4, 4, 4, 4, 4, 3, 2, 1, 0, -2, -4, -6, -6, -6, -5, -4, -3, -2, -1]
        t_expected = list(range(0, 240,
                                dt_us))  # the waveform does not end with zero, so we expect a zero point being added to the end

        le = LineEncoder.from_waveform(waveform, dt_us)
        # le.finalize()
        t, y = le.expand()

        wf_expected = waveform + [0]  # we expect a zero point to be added to the end of the waveform

        self.assertTrue((np.all(t == t_expected)))
        self.assertTrue(np.all(y == wf_expected))

    def test_from_differences(self):
        dt_us = 10

        t = list(range(0, 130, 10))
        #  t:         0,  10,  20,  30,  40,  50,  60,  70,  80,  90,  100,  110,  120,  130,  140,  150,  160,  170,  180,  190,  200
        waveform = [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, -1, -1, -1, -1, -1, -1, 0, 0, 1, 1]
        t_trans = [0, 30, 40, 70, 80, 100, 110, 160, 170, 180, 190, 200, 210]  # t coordinate of transition points
        y_trans = [0, 0, 1, 1, 0, 0, -1, -1, 0, 0, 1, 1, 0]  # y coordinate of transition points
        t_expected = [0, 30, 40, 50, 60, 70, 80, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200,
                      210]  # expected expanded waveform (time)
        y_expected = [0, 0, 1, 1, 1, 1, 0, 0, -1, -1, -1, -1, -1, -1, 0, 0, 1, 1, 0]  # expected expanded waveform (y)
        # Note that in the expanded waveform, sections where y=0 are compressed, i.e. only the first and last point of such
        # a segment are stored.

        # check that diffs are calculated correctly
        diffs = np.diff(waveform)
        diffs_expected = [0, 0, 0, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, 1, 0]
        # rle            [0, 0, 1, 1, 0, 0, 1, -1, 0, 0, 0, -1, 0, 0, 3,       1, 0, 1, 0]
        self.assertTrue(np.all(diffs == diffs_expected))

        # check parsing of diff-encoded waveform
        le = LineEncoder.from_differences(diffs, dt_us)
        self.assertTrue(np.all(le.t == t_trans))
        self.assertTrue(np.all(le.y == y_trans))

        # check expansion of encoded waveform
        t, y = le.expand()
        self.assertTrue(np.all(t == t_expected))
        self.assertTrue(np.all(y == y_expected))

    def test_from_rle_encoded_differences(self):
        dt_us = 10

        t = list(range(0, 130, 10))
        #  t:         0,  10,  20,  30,  40,  50,  60,  70,  80,  90,  100,  110,  120
        waveform = [0, 0, 0, 0, 10, 20, 30, 30, 0, -10, -10, -5, 0]
        t_trans = [0, 30, 60, 70, 80, 90, 100, 120]  # t coordinate of transition points
        y_trans = [0, 0, 30, 30, 0, -10, -10, 0]  # y coordinate of transition points
        t_expected = [0, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120]  # expected expanded waveform (time)
        y_expected = [0, 0, 10, 20, 30, 30, 0, -10, -10, -5, 0]  # expected expanded waveform (y)
        # Note that in the expanded waveform, sections where y=0 are compressed, i.e. only the first and last point of such
        # a segment are stored.

        # check that diffs are calculated correctly
        diffs = np.diff(waveform)
        diffs_expected = [0, 0, 0, 10, 10, 10, 0, -30, -10, 0, 5, 5]
        self.assertTrue(np.all(diffs == diffs_expected))

        # check run-length encoding
        rle_expected = [0, 0, 1, 10, 10, 1, 0, -30, -10, 0, 5, 5, 0]
        rle = LineEncoder.run_length_encode(diffs)
        self.assertTrue(np.all(rle == rle_expected))

        # check parsing of rl-encoded waveform
        le = LineEncoder.from_rle_encoded_differences(rle, dt_us)
        self.assertTrue(np.all(le.t == t_trans))
        self.assertTrue(np.all(le.y == y_trans))

        # check expansion of encoded waveform
        t, y = le.expand()
        self.assertTrue(np.all(t == t_expected))
        self.assertTrue(np.all(y == y_expected))

    def test_get_waveform(self):
        dt_us = 10
        waveform = [0, 1, 2, 3, 4, 4, 4, 4, 4, 3, 2, 1, 0, -2, -4, -6, -6, -6, -5, -4, -3, -2, -1, 0]
        t_expected = [0, 40, 80, 120, 150, 170, 230]
        y_expected = [0, 4, 4, 0, -6, -6, 0]

        le = LineEncoder.from_waveform(waveform, dt_us)

        t, y = le.get_waveform()

        self.assertTrue(np.all(t == t_expected))
        self.assertTrue(np.all(y == y_expected))

    def test_get_partial_waveform(self):
        dt_us = 10
        waveform = [0, 1, 2, 3, 4, 4, 4, 4, 4, 3, 2, 1, 0, -2, -4, -6, -6, -6, -5, -4, -3, -2, -1, 0]

        t_start = 30
        t_end = 220

        t_expected = [30, 40, 80, 120, 150, 170, 220]
        y_expected = [3, 4, 4, 0, -6, -6, -1]

        le = LineEncoder.from_waveform(waveform, dt_us)

        t, y = le.get_waveform(t_start, t_end)
        self.assertTrue(np.all(t == t_expected))
        self.assertTrue(np.all(y == y_expected))

    def test_addition(self):
        enc1 = LineEncoder(10, 1.0, t=[0, 100, 120, 300, 320], y=[0, 0, 10, 10, 0])
        enc2 = LineEncoder(10, 1.0, t=[0, 200, 210, 280, 290], y=[0, 0, -5, -5, 0])
        enc_add = enc1 + enc2

        x_expected = np.asarray([0, 100, 120, 200, 210, 280, 290, 300, 320])
        y_expected = np.asarray([0, 0, 10, 10, 5, 5, 10, 10, 0])
        self.assertTrue(np.all(enc_add.t == x_expected))
        self.assertTrue(np.all(enc_add.y == y_expected))

        enc_add2 = enc2 + enc1
        self.assertTrue(np.all(enc_add2.t == x_expected))
        self.assertTrue(np.all(enc_add2.y == y_expected))

    def test_addition_with_empty(self):
        x = [0, 100, 120, 300, 320]
        y = [0, 0, 10, 10, 0]
        enc1 = LineEncoder(10, 1.0, x, y)
        enc2 = LineEncoder(10, 1.0)
        enc_add = enc1 + enc2
        x_expected = x
        y_expected = y

        self.assertTrue(np.all(enc_add.t == x_expected))
        self.assertTrue(np.all(enc_add.y == y_expected))

        enc_add2 = enc2 + enc1
        self.assertTrue(np.all(enc_add2.t == x_expected))
        self.assertTrue(np.all(enc_add2.y == y_expected))

    def test_addition_nonoverlapping(self):
        """
        Test adding two encoders with non-overlapping waveforms. In this case, the __add__ method takes a shortcut
        by basically just concatenating the waveforms, so it is a different algorithm than for adding overlapping
        waveforms.
        :return:
        """
        t1 = [0, 100, 200, 800, 900]
        y1 = [0, 0, 30, 30, 0]
        t2 = [0, 1500, 1600, 2000, 2100]
        y2 = [0, 0, -10, -10, 0]

        t_expected = [0, 100, 200, 800, 900, 1500, 1600, 2000, 2100]
        y_expected = [0,   0,  30,  30,   0,    0,  -10,  -10,    0]

        enc1 = LineEncoder(10, 1, t1, y1)
        enc2 = LineEncoder(10, 1, t2, y2)

        enc12 = enc1 + enc2
        self.assertTrue(np.all(enc12.t == t_expected))
        self.assertTrue(np.all(enc12.y == y_expected))

        enc21 = enc2 + enc1
        self.assertTrue(np.all(enc21.t == t_expected))

        self.assertTrue(np.all(enc21.y == y_expected))

    def test_expand_empty(self):
        """
        When expanding an empty LineEncoder, it should return a list with one point (0, 0) only.
        :return:
        """
        enc = LineEncoder()
        x, y, = enc.expand()
        self.assertTrue(len(x) == 1)
        self.assertTrue(len(y) == 1)
        self.assertTrue(x[0] == 0)
        self.assertTrue(y[0] == 0)




if __name__ == '__main__':
    unittest.main()
