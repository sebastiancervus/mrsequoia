"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

import matplotlib.pyplot as plt
import numpy as np

from Atoms.AtomBase import AtomBase
from Atoms.AtomType import AtomType

from typing import Optional
from LineEncoder import LineEncoder


class GradientAtom(AtomBase):
    """
    GradientAtom: a trapezoidal gradient on one of the logical (i.e., PE, RO, or SS) axes, including its waveform. So
    far, only trapezoidal gradient shapes are supported. One gradient consists of a ramp-up, a plateau and a ramp-down.
    """

    def __init__(self, start_time: int, name: str, ampl: float, duration: int, rut: int, rdt: int, ax: AtomType,
                 timing: 'SequenceTiming' = None):
        """
        :param start_time: the absolute start time (relative to the start of the sequence), in µs
        :param name: the name/identifier of the atom
        :param ampl: the amplitude of the gradient, in mT/m
        :param duration: the duration, in µs (note that gradient duration excludes the ramp-down time)
        :param rut: ramp-up time, in µs
        :param rdt: ramp-down time, in µs
        :param ax: an AtomType indicating the logical axis of the gradient
        :param timing: a reference to the parent SequenceTimining object to query global information from, e.g., the
        gradient rotation matrix 
        """
        super().__init__(start_time, name, duration, timing)
        self.rut: int = rut
        self.rdt: int = rdt
        self.total_time: int = duration+rdt
        self.axis: AtomType = ax
        self.amplitude: float = ampl
        self.plateau_time:int = duration - self.rut
        self.rot_matrix: Optional[np.ndarray] = None  # Rotation matrix between logical and physical coordinates

    def __str__(self) -> str:
        """
        Generate a string represenation of the gradient
        :return: a string representation
        """
        return "Gradient with name %s, start_time %d, amplitude %.1f, duration %d, ramp-up time %d, " \
               "ramp-down time %d, axis %s" % (self.name, self.start_time, self.amplitude, self.duration, self.rut,
                                               self.rdt, self.axis.__str__())

    def to_html(self, additional_information: str = '') -> str:

        axis = ''

        if self.axis == AtomType.GR:
            axis = 'RO'
        elif self.axis == AtomType.GP:
            axis = 'PE'
        elif self.axis == AtomType.GS:
            axis = 'SS'

        add_info = 'ampl = %.2f mT/m, rut/ft/rdt = %d/%d/%d µs, axis = %s' % (self.amplitude, self.rut,
                                                                              self.plateau_time, self.rdt, axis)
        return super().to_html(additional_information=add_info)

    def get_center_time(self) -> int:
        """
        Get the center time of the gradient (total time, including ramp-down).
        Since all gradient durations are multiples of 10 us, we don't need to worry about non-integer numbers.
        :return: The timepoint for the center of the gradient, in µs
        """
        center_time = int((self.start_time + self.start_time+self.total_time)/2)
        return center_time

    def set_rotation_matrix(self, rot_matrix: np.ndarray):
        """
        Set the rotation matrix that translates from (PE, RO, SS) to (t, y, z) coordinates
        :param rot_matrix: a 3x3 rotation matrix (as numpy.ndarray)
        :return: Nothing.
        """
        self.rot_matrix = rot_matrix

    def get_waveform_trapezoidal(self, scanner_coordinates: bool = True) -> (np.ndarray, np.ndarray):
        """
        Calculate the shape of a trapezoidal gradient in scanner coordinates (t, y, z) or logical coordinates (PE,
        RO, SS). Note that the calculation is based on the trapezoidal represenation of the gradient, not on its
        actual waveform. This is necessary since in the simulator's waveform output, multiple gradients may overlap,
        and it is not possible to separate them. ;param scanner_coordinates: If set to True, scanner coordinates (t,
        y, z) will be used. Otherwise, the logical coordinate system will be used. Defaults to scanner coordinates.
        :return: a tuple (time, gradients) of two ND-arrays. time has size 1xN, and gradients is 4xN, where N is the
        number of sample points. In gradients, the first coordinate is (t, y, z, vector sum) or (RO, PE, SS,
        vector sum), depending on whether scanner coordinates or logical coordinates are being used.
        """
        if self.rot_matrix is None:
            if self.timing is not None:
                rot = self.timing.get_gradient_rotation_matrix()
                if rot.any():
                    self.rot_matrix = rot
                else:
                    print('Rotation matrix could not be determined. Using identity matrix.')
                    self.rot_matrix = np.eye(3)
            else:
                print('No sequence timinng specified, cannot retrieve rotation matrix. Using identity matrix')
                self.rot_matrix = np.eye(3)

        if not scanner_coordinates:  # plot in logical coordinates (PE, RO, SS) -> no rotation
            self.rot_matrix = np.eye(3)

        rot = self.rot_matrix

        dt = 10  # sampling period in us

        n_points = int(self.total_time / dt)

        grads = np.zeros((4, n_points))  # 4 rows: t, y, z, vector sum
        time = np.asarray(range(0, n_points))*dt

        ru_slope = self.amplitude / self.rut  # slope of the ramp-up
        rd_slope = self.amplitude / self.rdt  # slope of the ramp-down

        ru_points = int(self.rut/dt)
        rd_points = int(self.rdt/dt)
        ft_points = int((self.duration-self.rut)/dt)

        def vector_value(val: float) -> np.ndarray:
            """
            Convert a single amplitude value in logical coordinates to a vector of components in logical coordinates)
            :param val: the amplitude of the gradient on its logical axis
            :return: the t, y, z components of the gradient as a 3-component vector
            """

            if self.axis == AtomType.GP:
                return np.dot(np.asarray([val, 0, 0]), rot)
            elif self.axis == AtomType.GR:
                return np.dot(np.asarray([0, val, 0]), rot)
            elif self.axis == AtomType.GS:
                return np.dot(np.asarray([0, 0, val]), rot)
            else:
                print('Error: Unknown gradient axis')
                return np.asarray([0, 0, 0])

        rd_start_time = time[ru_points+ft_points-1]  # -1 since the first ramp-down point is already one step away from
        # the plateau height

        # next we build up the gradient shape point by point

        for k in range(0, ru_points):
            ampl = ru_slope*time[k]
            grads[0:3, k] = vector_value(ampl)
        for k in range(ru_points, ru_points+ft_points):
            grads[0:3, k] = vector_value(self.amplitude)
        for k in range(ru_points+ft_points, ru_points+ft_points+rd_points):
            t = time[k]-rd_start_time
            grads[0:3, k] = vector_value(self.amplitude - rd_slope*t)

        grads[3, :] = np.sqrt(np.sum(np.square(grads[0:3, :]), axis=0))
        return time, grads

    def plot_trapezoidal(self, scanner_coordinates: bool = True) -> None:
        """
        Plot the shape of a trapezoidal gradient in scanner coordinates (t, y, z) or logical coordinates (PE, RO,
        SS). Note that the plotting is based on the trapezoidal represenation of the gradient, not on its actual
        waveform. This is necessary since in the simulator's waveform output, multiple gradients may overlap,
        and it is not possible to separate them. ;param scanner_coordinates: If set to True, scanner coordinates (t,
        y, z) will be used. Otherwise, the logical coordinate system will be used. Defaults to scanner coordinates.
        :return: Nothing. Niente. Nada. Nix.
        """

        time, grads = self.get_waveform_trapezoidal(scanner_coordinates)

        fig, ax = plt.subplots(figsize=(20, 6))

        if scanner_coordinates:
            # calculate the vector sum of the gradients:
            grads[3, :] = np.sqrt(np.sum(np.square(grads[0:3, :]), axis=0))
            ax.plot(time, grads[0:3, :].transpose())
            ax.plot(time, grads[3, :], dashes=[5, 10], linewidth=4)
            ax.legend(['Gx', 'Gy', 'Gz', '|G|'], fontsize=16)
        else:
            ax.plot(time, grads[0:3, :].transpose())  # when plotting in logical coordinates, the vector sum is always
            # equalto one of the components (up to a sign), so no need to plot |G|
            ax.legend(['Gpe', 'Gro', 'Gss'], fontsize=16)

        ax.set_xlabel('time [µs]', fontsize=16)
        ax.set_ylabel('amplitude [mT/m]', fontsize=16)

    def to_mrilab_xml(self) -> str:
        """
        Return an XML representation of the gradient object that can be imported into MRiLab
        :return: the XML representation as a string
        """

        # example:
        # <GxTrapezoid DupSpacing="0" Duplicates="1" GxAmp="20" Notes="" Switch="$1'on','off'"
        #              sRamp="2"
        #              tEnd="VCtl.TE+100e-3"
        #              tRamp="50e-3"
        #              tStart="VCtl.TE"/>

        # Note: tStart and tEnd indicate the start and end time of the flat top; the gradient starts at tStart-tRamp and
        # ends at tEnd+tRamp!
        # All timing is in µs.
        # sRamp="100" indicates number sample points of the ramp (=2 ignores the area under the ramp)

        ft_start = '%.6f' % ((self.start_time + self.rut) * 1e-6)
        ft_end = '%.6f' % ((self.start_time + self.duration) * 1e-6)

        # MRiLab uses the convention t->RO, y->PE, z->SS
        axes = {AtomType.GR: 'Gx', AtomType.GP: 'Gy', AtomType.GS: 'Gz'}
        axis_str = axes[self.axis]

        t_ramp = '%.6f' % (self.rut * 1e-6)

        # number of sample points on the ramp: at least 2 (which will ignore the area under the ramp, otherwise every
        # 10 µs
        s_ramp = max(2, int(self.rut))
        # note that it is assumed that rut=rdt, since MRiLab does not provide a way to specify ramp-up and ramp-down
        # separately (unless when using user-defined gradient shapes

        # Gradient amplitude is specified in T/m in MRiLab
        amplitude = '%.4f' % (self.amplitude / 1000)

        xml = f"""<{axis_str}Trapezoid DupSpacing="0" Duplicates="1" {axis_str}Amp="{amplitude}" Notes="{self.name}" 
                      Switch="$1'on','off'"
                      sRamp="{s_ramp}"
                      tEnd="{ft_end}"
                      tRamp="{t_ramp}"
                      tStart="{ft_start}"/>
        """
        return xml

    def get_moment(self, order: int) -> float:
        """
        Get the moment of a given order for a gradient
        :param order: the order of the moment
        :return: the specified moment, in mT/m*s^(order+1)
        """

        if order == 0:
            return self.duration * self.amplitude  # for trapezoidal gradients
        else:
            print('Error: moments > 0 have not been implemented yet.')
            return 0

    def get_encoder(self) -> LineEncoder:
        """
        Generate an Encoder object that represents the waveform of the gradient
        :return: A LineEncoder object
        """

        # by convention, the first point of the encoder has to be at t=0. If the gradient starts later, we add this
        # manually before the proper waveform starts at self.start_time
        points_t = []
        points_y = []
        if self.start_time > 0:
            points_t += [0]
            points_y += [0]

        points_t += [self.start_time, self.start_time+self.rut, self.start_time+self.rut+self.plateau_time, self.start_time+self.total_time]
        points_y += [0,               self.amplitude,           self.amplitude,                             0]
        dt_us = 10
        return LineEncoder(dt_us, 1.0, points_t, points_y)

    def get_axis_as_string(self) -> str:
        if self.axis == AtomType.GS:
            return "SS"
        elif self.axis == AtomType.GP:
            return 'PE'
        elif self.axis == AtomType.GR:
            return 'RO'
        else:
            return "NA"  # should never happen, really!

    def is_trapezoidal(self) -> bool:
        """
        Returns True if the gradient is trapezoidal, and False otherwise.
        TBD: For not this is a dummy function, since non-trapezoidal gradients are not fully supported yet.
        :return: whether the gradient is trapezoidal or not (bool)
        """
        return True

    def to_xml_trapezoidal(self) -> str:

        axis = ''
        if self.axis == AtomType.GR:
            axis = 'Gx'
        elif self.axis == AtomType.GP:
            axis = 'Gy'
        else:
            axis = 'Gz'

        ampl = self.amplitude/1000
        notes = self.name
        #sramp = self.rut/10 + 1  # number of sampling points on the ramp, make sure that this is at least 3 to consider the area under the ramp
        sramp = int(self.rut)  # 1 point per µs
        t_start = (self.start_time + self.rut)/1e6  # the start time (which is the start of the plateau time
        t_end = (self.start_time + self.rut + self.plateau_time)/1e6  # end time (end of the plateau)
        t_ramp = self.rut / 1e6

        xml = f'''<{axis}Trapezoid DupSpacing="0" Duplicates="1" {axis}Amp="{ampl:.10f}" Notes="{notes}" Switch="$1'on','off'"
                      sRamp="{sramp}"
                      tEnd="{t_end:.9f}"
                      tRamp="{t_ramp:.9f}"
                      tStart="{t_start:.9f}"/>
'''
        return xml

