"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from enum import IntEnum


class AtomType(IntEnum):
    """
    AtomType: an enum class that maps the cells from the realtime event block table in the _INF.dsv file to atom types.
    The indices refer to the column indices of that table (0-based).
    """
    TX = 2
    FreqPhase = 3
    PRS = 4
    ADC = 5
    GP = 6
    GR = 7
    GS = 8
    SYNC = 9
    GX = 10
    GY = 11
    GZ = 12
