"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from .AtomBase import AtomBase


class SyncAtom(AtomBase):
    """
    SyncAtom: a sync (trigger pulse) atom.
    """
    def __init__(self, start_time: int, name: str, duration: int, channel: str, timing):
        """
        Constructor for a SyncAtom.
        :param start_time: the absolute start time (relative to the start of the sequence), in µs
        :param name: the name/identifier of the atom
        :param duration: the duration, in µs (note that gradient duration excludes the ramp-down time)
        :param channel: the trigger channel index (integer)
        :param timing: a reference to the parent SequenceTimining object to query global information from
        """
        super().__init__(start_time, name, duration, timing)
        self.channel: str = channel

    def __str__(self) -> str:
        """
        Generate a string representation of this atom
        :return: a string representation
        """
        return "Sync with name %s, start_time %d, duration %d, channel %s" % (self.name, self.start_time, self.duration,
                                                                              self.channel)

    def to_html(self) -> str:
        add_info = 'channel = %s' % self.channel
        return super().to_html(additional_information=add_info)
