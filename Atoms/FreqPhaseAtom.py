"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from .AtomBase import AtomBase


class FreqPhaseAtom(AtomBase):
    """
    FreqPhaseAtoms: a representation of a TX/RX freq/phase adjustment.
    """

    def __init__(self, start_time: int, name: str, freq: float, phase: float, timing):
        """
        The constructor for FreqPhaseAtoms. FreqPhaseAtomss do not have a duration, so the duration property is set to
        0
        :param start_time: the absolute start time (relative to the start of the sequence), in µs
        :param name: the name/identifier of the atom
        :param freq: the frequency offset (in Hz)
        :param phase: the phase offset (in degrees)
        :param timing: a reference to the parent SequenceTimining object to query global information from, e.g., the
        gradient rotation matrix
        """
        super().__init__(start_time, name, 0, timing)
        self.freq: float = freq
        self.phase: float = phase

    def __str__(self) -> str:
        """
        Generate a string representation of the atom
        :return: a string representation
        """
        return "FreqPhase with name %s, start_time %d, frequency %.2f, phase %.2f" % (self.name, self.start_time,
                                                                                      self.freq, self.phase)

    def to_html(self) -> str:
        add_info = 'f = %.2f Hz, phase = %.1f°' % (self.freq, self.phase)
        return super().to_html(additional_information=add_info)
