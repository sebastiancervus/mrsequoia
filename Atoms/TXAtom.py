"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from typing import Tuple, Optional

import matplotlib.pyplot as plt
import numpy as np
import warnings

# from SiemensTools.SiemensWaveformManager import SiemensWaveformManager
from Atoms.AtomBase import AtomBase
from math import pi as PI
from LineEncoder import LineEncoder
from Encoder import Encoder
from MRConstants import gamma


class TXAtom(AtomBase):
    """
    TXAtom: A class representing a single (possibly multi-channel) RX pulse, including waveforms of the
    RF magnitude and phase.
    """
    dt_us: int = 1  # the time resolution, in µs
    tesla_to_volts = 2000 * 42.58 * 100

    def __init__(self, start_time: int, name: str, angle: float, duration: int, timing: 'SequenceTiming'):
        """
        Constructor for TXAtom
        :param start_time: the absolute start time (relative to the start of the sequence), in µs
        :param name: the name/identifier of the pulse
        :param angle: the flip angle, in degrees
        :param duration: the pulse duration, in µs
        :param timing: a reference to the parent SequenceTimining object to query global information from
        """
        super().__init__(start_time, name, duration, timing)

        self.angle: float = angle
        self.waveform0: Optional[LineEncoder] = None
        self.waveform1: Optional[LineEncoder] = None
        self.phase0: Optional[LineEncoder] = None
        self.phase1: Optional[LineEncoder] = None
        self.offset_freq: float = 0  # frequency offset, in Hz
        self.offset_phase: float = 0  # the static offset phase (constant for all occurences of this pulse), in degree
        self.dynamic_phase: float = 0  # dynamic offset phase (only for next execution), in degree
        # the total phase is calculated as offset_phase + dynamic_phase as a property function

    def __str__(self) -> str:
        """
        Generate a string representation of the pulse
        :return: a string representation
        """
        return 'TX with name %s, start_time %d, angle %.1f, duration %d' % (self.name, self.start_time, self.angle,
                                                                            self.duration)

    def to_html(self) -> str:
        add_info = 'angle = %.1f°' % self.angle
        return super().to_html(additional_information=add_info)

    def set_encoders(self, wf0: Encoder = None, wf1: Encoder = None,
                     phase0: Encoder = None, phase1: Encoder = None) -> None:
        """
        Set the waveform managers representing amplitude and phase of the two RF channels during the pulse
        :param wf0: amplitude of the first channel
        :param wf1: amplitude of the second channel
        :param phase0: phase of the first channel
        :param phase1: phase of the second channel
        :return: nothing
        """
        self.waveform0 = wf0
        self.waveform1 = wf1
        self.phase0 = phase0
        self.phase1 = phase1

    @property
    def total_phase(self) -> float:
        return self.offset_phase + self.dynamic_phase

    def get_waveform(self, channel: int, start_with_zero_time: bool = False) -> Tuple[
        np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """
        Return the time axis, amplitude and phase for the specified RF channel
        :param channel: channel index (0 or 1)
        :param start_with_zero_time: if True, the time axis starts with zero at the start of the pulse. If False, the
        time axis represents the timing within the sequence, and the time vector starts with self.start_time.
        :return: a tuple (time, magnitude, phase) consisting of 3 one-dimensional ndarrays.
        """
        if not 0 <= channel < 2:
            print('Error. The waveform channel has to be 0 or 1')
            return None, None, None

        if channel == 0:
            wm = self.waveform0
            pha = self.phase0
        else:
            wm = self.waveform1
            pha = self.phase1

        if wm is None:
            print('Waveform does not exist.')
            return None, None, None

        # time, magnitude = wm.get_interval(self.start_time, self.start_time+self.duration)
        time, magnitude = wm.get_waveform(self.start_time, self.start_time + self.duration)

        phase: np.ndarray = None
        pha_time: np.ndarray = None
        if pha is not None:
            # pha_time, phase = pha.get_interval(self.start_time, self.start_time+self.duration)
            pha_time, phase = pha.get_waveform(self.start_time, self.start_time + self.duration)

            # if pha_time.size > 1:

            # unwrap the phase:
            # phase = unwrap(phase)

            # rf and nc have different time scales in the output file, so we need to undersample
            # the phase here to make the resolutions match:
            # dt_pha = pha_time[1]-pha_time[0]
            # dt_pha = pha.encoder.dt_us
            # dt_mag = time[1]-time[0]
            # dt_mag = wm.encoder.dt_us

            # us_factor = int(dt_mag/dt_pha)  # undersampling factor

            # we take the magnitude as the reference data. Find the point within phase that matches
            # the first magnitude point
            # first = np.flatnonzero(pha_time == time[0])[0]
            # ind = range(first, pha_time.size, us_factor)

            # phase = phase[ind]
        else:
            print('No phase information available for channel ' + str(channel) + '. Using magnitude only.')

        if phase is None:
            phase = np.zeros(magnitude.shape)

        if start_with_zero_time:
            time = time - time[0]

        return time, magnitude, pha_time, phase

    def set_offset_freq_phase(self, df: float, dp: float):
        """
        Set the offset frequency and phase (in Hz and degree):
        :param df: offset frequency in Hz
        :param dp: offset phase in degree
        :return: nothing
        """
        self.offset_freq = df
        self.offset_phase = dp

    def plot_waveform(self) -> None:
        """
        Generate a plot of the magnitude and phase of the two RF channels
        :return: nothing
        """

        t, mag0, pha_t, pha0 = self.get_waveform(0)

        fig, ax = plt.subplots(figsize=(10, 6))
        plt.subplots_adjust(left=0.1, bottom=0.01, right=None, top=0.1, wspace=0.2, hspace=0.1)
        fig.suptitle('TX pulse: ' + self.name)

        color_magn1 = 'tab:blue'
        color_pha1 = 'tab:red'

        ax.plot(t, mag0, color=color_magn1, label='ampl ch0')
        ax.set_xlabel('time [µs]', fontsize=12)
        ax.set_ylabel('TX amplitude', color=color_magn1, fontsize=12)
        ax.tick_params(axis='y', labelcolor=color_magn1)

        handles, labels = ax.get_legend_handles_labels()

        if pha_t is not None and pha0 is not None:
            ax2 = ax.twinx()
            ax2.plot(pha_t, pha0, color=color_pha1, label='pha ch0')
            ax2.set_ylabel('TX phase [rad]', color=color_pha1, fontsize=12)
            ax2.tick_params(axis='y', labelcolor=color_pha1)

            h2, l2 = ax2.get_legend_handles_labels()
            handles += h2
            labels += l2

        ax.legend(handles, labels)
        fig.tight_layout()

    def to_mrilab_xml(self, filename: str) -> str:

        xml = f"""
        <rfUser AnchorTE="$2'on','off'" CoilID="1" DupSpacing="0" Duplicates="1"
                 Notes="{self.name}"
                 Switch="$1'on','off'"
                 rfFile="'{filename}'"
                 />
        """

        return xml

    def set_start_time(self, start_time: float) -> None:
        self.start_time = start_time
        if self.waveform0 is not None:
            self.waveform0.set_start_time(start_time)
        if self.waveform1 is not None:
            self.waveform1.set_start_time(start_time)
        if self.phase0 is not None:
            self.phase0.set_start_time((start_time))
        if self.phase1 is not None:
            self.phase1.set_start_time(start_time)

    @classmethod
    def sinc_pulse(cls, start_time: int = 0, angle: float = 90, duration: int = 5120, initial_phase: float = 0) -> 'TXAtom':
        """
        Generate a TXAtom representing a sinc excitation pulse with specified angle and duration This code is based
        on the following publication: Giovannetti, G., Frijia, F., Flori, A., De Marchi, D., Aquaro, G. D.,
        Menichetti, L., & Ardenkjaer-Larsen, J. H. (2015). A fast and simple method for calibrating the flip angle in
        hyperpolarized 13C MRS experiments. Concepts in Magnetic Resonance Part B: Magnetic Resonance Engineering,
        45(2), 78–84. https://doi.org/10.1002/cmr.b.21282
        :param start_time: the start time of the pulse, in µs
        :param angle: the flip angle, in degrees
        :param duration: the duration of the pulse, in µs
        :param initial_phase: the initial complex phase of the pulse, in degrees TODO
        :param ref_voltage: the reference voltage parameter used in the Siemens simulation. If set to zero, the
        value will be adjusted to that the exported pulse forms (Using MRiLab exporter) are in µT.
        :return: A TXAtom object
        """

        #   ampl_ups = 90 / (500e-6 * 360 * 42.58e6) * ampl_ups / self.ref_voltage
        # the conversion between the amplitude in Volts (used by the Siemens simulation) and T (used by MRiLab) is
        # ampl_T = 90° / (360° * 500 µs * 360° * 42.58 MHz/T) * ampl_V/ref_voltage
        # ampl_T = 1 / (2000*42.58) * ampl_V/ref_voltage
        # Here we do the calculation in T, but since the MRiLabExporter assumes the pulse amplitude in Volts, we scale
        # the value corresponding to a reference voltage of 100 V, which is the default value used by MRiLabExporter
        # if no value is specified explicitly.
        tesla_to_volts = 2000 * 42.58 * 100

        time_bw = 4  # time-bandwidth product
        angle_rad = angle / 180 * PI
        tau = duration*1e-6  # pulse duration in s
        r = 1/4  # correction factor for sinc pulses, from publication. Is this time-bandwidth product?

        B1max = angle_rad/(gamma*tau*r)

        n_points = round(duration / cls.dt_us)

        t_step = duration / n_points

        t = np.arange(0, duration + t_step, t_step)  # t is in µs!
        # Formula: Handbook of MRI pulse sequences, pg. 38
        zc = time_bw / 2  # number of zero-crossings on either side of the pulse center
        t0 = duration / 2 / zc  # time between zero crossings

        t_center = t - duration / 2.0  # shifted time axis with t=0 at the pulse center

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            b1_ampl = B1max * t0 * np.sin(PI * t_center / t0) / (PI * t_center)  # b0_ampl is now in µT
        b1_ampl[t_center == 0] = B1max  # deal with the center of the pulse, where division by zero occurs

        #b1_volts = np.asarray([cls.b1_to_volts(x, ref_voltage) for x in b1_ampl])
        b1_volts = tesla_to_volts * b1_ampl

        phase = np.zeros(t.shape)

        tx = TXAtom(start_time, 'Sinc', angle, duration, None)
        wf_ampl = LineEncoder(cls.dt_us, t=t + start_time, y=b1_volts)
        wf_phase = LineEncoder(cls.dt_us, t=t + start_time, y=phase)

        wf_ampl1 = LineEncoder(cls.dt_us)
        wf_phase1 = LineEncoder(cls.dt_us)

        # if the amplitude at the last point of wf_ampl is not zero, add a zero point
        if wf_ampl.y[-1] != 0:
            wf_ampl.t = np.append(wf_ampl.t, [wf_ampl.t[-1] + cls.dt_us])
            wf_ampl.y = np.append(wf_ampl.y, [0])
            wf_phase.t = np.append(wf_phase.t, [wf_phase.t[-1] + cls.dt_us])
            wf_phase.y = np.append(wf_phase.y, [0])

        # print(wf_phase.y)

        tx.set_encoders(wf_ampl, wf_ampl1, wf_phase, wf_phase1)
        #tx.bw_Hz =  1/(2*time_bw*duration*1e-6)
        tx.bw_Hz = time_bw/(duration*1e-6)
        tx.offset_phase = initial_phase
        return tx

    @classmethod
    def rect_pulse(cls, start_time: int = 0, angle: float = 90, duration: int = 5120, initial_phase: float = 0) -> 'TXAtom':
        """
        Create a hard rectangular pulse
        :param start_time: the start time, in µs
        :param angle: the flip angle, in degrees
        :param duration: the pulse duration, in µs
        :param initial_phase: the initial phase of the pulse, in degrees (not implemented yet)
        :return: a TXAtom instance with the desired properties
        """


        # flip angle = gamma * B1[T] * duration  (angle is then in rad)
        B1_uT = angle/180*PI / gamma / (duration*1e-6) * 1e6
        B1_V = B1_uT * cls.tesla_to_volts
        
        n_points = round(duration / cls.dt_us)
        t_step = duration / n_points
        t = np.arange(0, duration + t_step, t_step)  # t is in µs!

        y_ampl = np.ones(t.shape) * B1_V
        y_phase = np.zeros(t.shape)  # TODO

        t = t + start_time
        wf_ampl = LineEncoder(cls.dt_us, t=t, y=y_ampl)
        wf_phase = LineEncoder(cls.dt_us, t=t, y=y_phase)

        # second rf channel will be set to zeros:
        wf_ampl1 = LineEncoder(cls.dt_us)
        wf_phase1 = LineEncoder(cls.dt_us)

        tx = TXAtom(start_time, 'Rect', angle, duration, None)
        tx.set_encoders(wf0=wf_ampl, phase0=wf_phase, wf1=wf_ampl1, phase1=wf_phase1)
        return tx

    @classmethod
    def gaussian_pulse(cls, start_time:int = 0, angle: float = 90, duration: int = 5120, initial_phase: float = 0, sigma: float = 0) -> 'TXAtom':
        """
        Create a Gaussian excitation pulse
        :param start_time: the start time, in µs
        :param angle: the flip angle, in degrees
        :param duration: the pulse duration, in µs
        :param initial_phase: the initial pulse phase, in degrees (TBD)
        :param sigma: the standard deviation of the pulse shape, in µs (default: chosen such that the pulse is attenuated by -60dB at the edges).
        :return: a TXAtom instance with the desired properties
        """

        # Calculations according to Handbook of MRI pulse sequences, chapter 4

        # if the pulse width is not specified, calculate is such that the first and last point of the pulse waveform
        # are 1/1000 of the peak value (60 dB attenuation)
        if sigma == 0:
            sigma = duration / 7.434

        n_points = round(duration / cls.dt_us)
        dt = duration / n_points

        t = np.arange(0, duration + dt, dt) - duration/2  # t is in µs! Pulse is centered about t=0 for now
        amplitude = (angle/180*PI) / (gamma*sigma*np.sqrt(2*PI))

        envelope = amplitude * np.exp(-t**2/(2*sigma**2))

        # shift time vector back to [0...duration]
        t = t + duration/2

        b1_volts = amplitude * cls.tesla_to_volts

        phase = np.zeros(t.shape)  # TODO

        wf_ampl = LineEncoder(cls.dt_us, t=t, y=b1_volts)
        wf_phase = LineEncoder(cls.dt_us, t=t, y=phase)

        # second rf channel will be set to zeros:
        wf_ampl1 = LineEncoder(cls.dt_us)
        wf_phase1 = LineEncoder(cls.dt_us)

        tx = TXAtom(start_time, 'Gaussian', angle, duration, None)
        tx.set_encoders(wf0=wf_ampl, phase0=wf_phase, wf1=wf_ampl1, phase1=wf_phase1)
        return tx

    @staticmethod
    def b1_to_volts(b1_uT: float, ref_voltage: float):
        return b1_uT/11.7 * ref_voltage

    @staticmethod
    def b1_to_uT(b1_V: float, ref_voltage: float):
        return 11.7*b1_V/ref_voltage


def main():
    tx = TXAtom.rect_pulse(0, 15, 100)
    tx.set_start_time(300)



if __name__ == '__main__':
    main()
