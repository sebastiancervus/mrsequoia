#!/bin/bash

data_dir_source="$HOME/MRSequoia_data"
port=20000

echo ""
echo "Using $data_dir_source as data directory"
echo "To use MRSequoia in jupyter, direct your browser to http://127.0.0.1:$port"
echo ""

run_command="start.sh jupyter lab --port=$port"

docker run -it -p $port:$port -v $data_dir_source:/home/jovyan/MRSequoia_data -e USERID=$UID mrsequoia $run_command
