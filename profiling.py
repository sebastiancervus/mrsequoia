import cProfile


def fun1():
    from SeqTree.SequenceBuilder import SequenceBuilder
    from SeqTree.ActionNode import ActionNode
    from SeqTree.LoopNode import LoopNode
    from SeqTree.DelayNode import DelayNode
    from Atoms.TXAtom import TXAtom
    from Atoms.GradientAtom import GradientAtom
    from Atoms.AtomType import AtomType
    from SequenceDiagram import SequenceDiagram
    from SeqTree.ParameterizedFunction import ParameterizedFunction
    from SeqTree.PhaseEncoderNode import PhaseEncoderNode

    sb = SequenceBuilder()

    med = sb.mediator

    n_pe = 64
    pe_loop = LoopNode.pe_loop(med, n_pe, 100000)
    ss_grad = sb.gradient_node('SS-grad', 10, 1000, 300, 300, AtomType.GS)
    ampl_fun = ParameterizedFunction(ss_grad, lambda x: x[0], ['pe_idx'])

    pe_grad = PhaseEncoderNode('PE-grad', med, 10000, 0.128, n_pe, 500, 500, 'pe_idx')
    pe_rev_grad = PhaseEncoderNode('PE-rewinder', med, 10000, 0.128, n_pe, 500, 500, 'pe_idx', rewinder=True)

    tx_atom = TXAtom.sinc_pulse(0, 30, 5120, 0, 100)
    exc_comp = sb.excitation_node('Excitation', tx_atom, 5e-3)

    ro_comp = sb.readout_node('Readout_comp', 0.128, n_pe, 500, 100)

    sb.add_child(sb.root, pe_loop)
    sb.add_child(pe_loop, exc_comp)
    sb.add_child(pe_loop, pe_grad)
    sb.add_child(pe_loop, ro_comp)
    sb.add_child(pe_loop, pe_rev_grad)

    st = sb.build_sequence()
    #sb.plot_tree()

def fun2():
    from LineEncoder import LineEncoder

    enc1 = LineEncoder()
    enc2 = LineEncoder()

    enc1.t = [0, 100, 900, 1000]
    enc1.y = [0, 20, 20, 0]

    enc2.t = [0, 2000, 2100, 2900, 3000]
    enc2.y = [0, 0, 30, 30, 0]

    enc = enc1 + enc2
    print(enc.t)
    print(enc.y)


def main():
    #pr = cProfile.Profile()
    #pr.enable()
    fun2()
    #pr.disable()
    # after your program ends
    #pr.print_stats(sort="calls")

if __name__ == '__main__':
    main()