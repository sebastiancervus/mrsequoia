available_sources = [('RF0', 'rf0'), ('RF1', 'rf1'), ('GRX', 'grx'), ('GRY', 'gry'), ('GRZ', 'grz'), ('ADC', 'adc'),
                     ('PHA0', 'rfpha0')]


def get_source_by_id(id: str):
    for x in available_sources:
        if x[0] == id:
            return x
    return None


# color definitions for the plots. Uses the "CN" color definition of matplotlib, see
# https://matplotlib.org/3.1.1/tutorials/colors/colors.html
line_colors = dict()

_c = 1
for x in available_sources:
    line_colors[x[0]] = 'C'+str(_c)
    _c += 1
