"""
Written by Sebastian Hirsch.
sebastian.hirsch@bccn-berlin.de
See LICENSE.txt for licensing information.
"""

from types import FunctionType


class AtomList(list):
    """
    AtomList: an extension of the built-in list class that supports multiple types of access:
    - index-based as the original list class: atom_list[3] -> single object
    - index-based with slices: atom_list[0:2] -> AtomList
    - based on the .name attribute of the objects: atom_list['Excitation'] returns an AtomList of all object with name
     'Excitation'
    - based on a function/lambda with boolean return value:
        selector = lambda t: t.duration > 3
        atom_list[selector]
        returns an AtomList with all objects satisfying the condition
    """

    def __getitem__(self, key):

        if isinstance(key, int):
            return super(type(self), self).__getitem__(key)
        if isinstance(key, slice):
            res = super(AtomList, self).__getitem__(key)
            return AtomList(res)
        elif isinstance(key, str):
            res = AtomList([x for x in self if x.name == key])
            return res
        elif isinstance(key, FunctionType):
            res = AtomList([x for x in self if key(x)])
            return res
        else:  # this will probably always result in a TypeError, since we covered all relevant cases above
            print(f'Executing the else branch of AtomList.__getitem__ with argument type {type(key)}. This is unexpected!')
            try:  # this is wishful thinking and will not happen, therfore excluded from testing # pragma: no cover
                res = AtomList(super(type(self), self).__getitem__(key))
                return res
            except TypeError as te:
                raise te
