"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from typing import Tuple, List, Optional

import matplotlib
import matplotlib.axes as axes
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from IPython.display import display
from ipywidgets import widgets

import DiagramOptions as options
from SequenceTiming import SequenceTiming
from TimeSeries import TimeSeries
import warnings


class SequenceDiagram:
    """
    SequenceDiagram: a class for plotting sequence diagrams.
    """

    print('Note: To use interactive plotting widgets, you have to execute "%matplotlib widget" in JupyterLab before '
          'running a plotting command')
    sns.set_context('notebook')
    matplotlib.rcParams['figure.figsize'] = (14, 4)

    def __init__(self, st: SequenceTiming, cont_update: bool = False):

        self.st: SequenceTiming = st
        #self.st.read_waveforms()

        self.t: Optional[np.ndarray] = None
        self.rf: Optional[np.ndarray] = None
        self.g1: Optional[np.ndarray] = None
        self.g2: Optional[np.ndarray] = None
        self.g3: Optional[np.ndarray] = None
        self.adc: Optional[np.ndarray] = None

        self.time_series: List[TimeSeries] = []

        self.ylims: List[Tuple[float, float]] = []

        self.slider_steps: int = 10000
        self.last_center_time: Optional[int] = None

        self.tmin = 0
        self.tmax = 0

        self.fig = None
        self.axes: Optional[np.ndarray] = None

        self.sources: List[str] = ['RF0']

        self.window_width = 1.0  # 1 indicates to display the entire data range.
        self.window_center = 0.5

        self.n_diagrams = 1  # the number of diagrams (i.e., how many data sources are selected)

        self.center_slider = widgets.FloatSlider(min=0, max=1, description='Window center', value=0.5,
                                                 layout=widgets.Layout(width='100%'), step=1/self.slider_steps,
                                                 continuous_update=cont_update)

        self.zoom_slider = widgets.FloatSlider(min=1/self.slider_steps, max=1, description='Zoom', value=0,
                                               layout=widgets.Layout(width='100%'), step=1/self.slider_steps,
                                               continuous_update=cont_update)

        self.html_widget = widgets.HTML(value='<b>Test</b>')

        # Some GUI buttons
        self.zoom_plus_button = widgets.Button(description='+')
        self.zoom_plus_button.on_click(self.increase_zoom)
        self.zoom_minus_button = widgets.Button(description='-')
        self.zoom_minus_button.on_click(self.decrease_zoom)
        self.pan_left_button = widgets.Button(description='<-')
        self.pan_left_button.on_click(self.pan_left)
        self.pan_right_button = widgets.Button(description='->')
        self.pan_right_button.on_click(self.pan_right)

        self.source_selector = widgets.SelectMultiple(description="Source (Ctrl+click for multiple)",
                                                      options=[x[0] for x in options.available_sources],
                                                      value=[options.available_sources[0][0]],
                                                      layout=widgets.Layout(width='100%'),
                                                      style={'description_width': 'initial'})

        self.zoom_slider_box = widgets.HBox([self.zoom_minus_button, self.zoom_slider, self.zoom_plus_button])
        self.center_slider_box = widgets.HBox([self.pan_left_button, self.center_slider, self.pan_right_button])

        self.split_time_points: list = []

    def plot(self):

        self.init_figure()

        self.update_ydata()

        self.tmin = 0.0
        self.tmax = 1.0

        self.update_ylims()

        ui = widgets.VBox([self.center_slider_box, self.zoom_slider_box, self.html_widget, self.source_selector])
        output = widgets.interactive_output(self.update_plot, {'tcenter': self.center_slider, 'tzoom': self.zoom_slider,
                                                               'sources': self.source_selector})
        return display(ui, output)

    def update_plot(self, tcenter, tzoom, sources=[options.available_sources[0][0]]):

        if len(sources) != self.n_diagrams:
            self.n_diagrams = len(sources)
            self.init_figure()

        if self.check_if_sources_list_was_changed(sources):
            self.sources = sources
            self.update_ydata()

        min_time_us = 50
        #total_time = self.time_series[0].get_total_time()
        total_time = max([x.get_total_time() for x in self.time_series])

        # scale the zoom level such that for zoom=1, min_points are displayed
        # we have the following relationships between zoom and the relative window width:
        # zoom | rel window width
        #   0  |       1 (show entire range)
        #   1  |       min_points/n_points  (min_points fit into the window)

        # scale: a mapping (0, 1) -> (0, 1) that translates tzoom into the actual zoom level
        scale = 1-(tzoom-1)**2  # quadratic scaling seems to work quite nicely.

        # in the case of an empty diagram, total_time is zero, so we need to avoid dividing by it
        if total_time > 0:
            width = 1 - (1 - min_time_us / total_time) * scale
        else:
            width = scale
        self.window_width = width

        halfwidth = width / 2

        if tcenter - halfwidth < 0:
            tcenter = halfwidth
            tmin = 0
            tmax = width
        elif tcenter + halfwidth > 1:
            tcenter = 1 - halfwidth
            tmax = 1
            tmin = 1 - width
        else:
            tmax = tcenter + halfwidth
            tmin = tcenter - halfwidth

        self.window_center = tcenter

        self.center_slider.value = tcenter

        tmin = round(total_time*tmin)
        tmax = round(total_time*tmax)

        center_time = int(round((tmin+tmax)/2.0))

        if center_time != self.last_center_time:
            self.last_center_time = center_time

            curr_events = self.st.get_atoms_at_time(center_time)
            # TODO: Filter curr_events so that it only contains events from the data sources that are currently being
            #  displayed

            event_html = '\n'.join(['<tr>' + x.to_html() + '</tr>' for x in curr_events])
            self.update_output(event_html)

        self.update_ylims()

        for i in range(0, self.n_diagrams):

            ax: axes = self.axes.item(i)  # ax is an nparray of SubplotAxes objects. However, if ax only has a single element, ax[0] generates an error. Therefore, we need to use the item method to extract an axis object and use it later on.
            ax.clear()

            for n in range(0, self.time_series[i].n()):  # iterate over time series for this data source

                ts_t, ts_v = self.time_series[i].get_series(n)
                t, vals = self.select_data(ts_t, ts_v, tmin, tmax)

                color = options.line_colors.get(self.sources[i], 'b')  # use blue as the default color

                ax.plot(t, vals, color)

            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                ax.set_ylim(self.ylims[i])
            ax.plot([center_time, center_time], [self.ylims[i][0], self.ylims[i][1]])  # plot vertical center line
            ax.title.set_text(self.sources[i])

    def init_figure(self) -> None:
        """
        If self.fig is None, initialize a new figure with self.n_diagrams subplots. If self.fig exists, clear its
        contents and initialize n_diagrams empty subplots
        :return: nothing
        """
        if self.fig is None:
            self.fig, ax = plt.subplots(nrows=self.n_diagrams, ncols=1, sharex=True)

        else:
            self.fig.clear()
            ax = self.fig.subplots(nrows=self.n_diagrams, ncols=1, sharex=True)

        # if only one axis was created, it is not returned as a list, so we need to convert it here.
        self.axes = ax if isinstance(ax, np.ndarray) else np.asarray(ax)

    def update_ylims(self) -> None:
        """
        Update self.ylims for all data series that are currently displayed
        :return: nothing
        """
        self.ylims = []
        for i in range(0, self.n_diagrams):
            mmin = self.time_series[i].min(0)
            mmax = self.time_series[i].max(0)
            am = 1.05 * max(np.abs(mmin), np.abs(mmax))  # absolute maximum + 5%
            ymin = min([np.sign(mmin)*am, 0])
            ymax = max([np.sign(mmax)*am, 0])
            self.ylims += [(ymin, ymax)]

    def update_ydata(self) -> None:
        """
        Update self.ydata if the source selection was changed. Make sure that this function only gets called when
        the list of sources has changed.
        :return: nothing
        """

        self.time_series = []

        splits = self.split_time_points
        if len(splits) == 0:
            splits = [0]  # make sure that there is at least one start point in the list of splitting time points

        time_offset = splits[0]

        for s in self.sources:
            source_tuple = options.get_source_by_id(s)  # format: (id, filename_part), where filename_part
            if source_tuple is None:
                print('Unknown data source: ' + s)
                continue

            wf_name = ''
            try:
                wf_name = 'wf_' + source_tuple[1]  # name of the WaveformManager object in SequenceTiming

                wf = getattr(self.st, wf_name)  # get the Encoder object for the data source

                ts = TimeSeries()
                for i in range(0, len(splits)):
                    start_time = splits[i]
                    if i+1 < len(splits):
                        end_time = splits[i+1]
                    else:
                        end_time = -1

                    t, v = wf.get_waveform(start_time, end_time)
                    t -= start_time  # subtract the start time to make sure that all _repetitions are aligned
                    t += time_offset  # this ensure that the timing for the first repetition is correct, for example
                    # for identifying objects based on their start time
                    ts.add_data(t, v)
                    
                self.time_series.append(ts)

            except NameError:
                print('Could not get WaveformManager ' + wf_name)

    def check_if_sources_list_was_changed(self, sources: list) -> bool:

        if len(sources) != len(self.sources):
            self.sources = sources
            return True

        for x in range(0, len(sources)):
            if sources[x] != self.sources[x]:
                return True
        
        return False

    def increase_zoom(self, val):  # for some reason, the callback functions for buttons must take one argument
        v = self.zoom_slider.value
        v = min(1.0, v + 0.01)  # we increase in steps of 1%
        self.zoom_slider.value = v

    def decrease_zoom(self, val: float): # for some reason, the callback functions for buttons must take one argument
        v = self.zoom_slider.value
        v = max(0.0, v - 0.01)  # we increase in steps of 1%
        self.zoom_slider.value = v

    def pan_left(self, val):
        v = self.center_slider.value
        # we want to move by 5% of the window width, if possible

        v = max(self.window_width/2, v-self.window_width/20)
        self.center_slider.value = v
       # self.update_display()

    def pan_right(self, val):
        v = self.center_slider.value
        # we want to move by 5% of the window width, if possible

        v = min(v+self.window_width/20, 1-self.window_width/2)
        self.center_slider.value = v

    def update_display(self):
        self.update_plot(tcenter=self.center_slider.value, tzoom=self.zoom_slider.value)

    def update_output(self, body: str):

        header = '''
        <table style="width:100%" border="1">
            <tr>
                <th>type</th>
                <th>name</th>
                <th>start time (µs)</th>
                <th>total time (µs)</th>
                <th>add. information</th>
            </tr>
        '''
        footer = '''
        </table>
        '''

        self.html_widget.value = header + body + footer

    @classmethod
    def select_data(cls, t: np.ndarray, y: np.ndarray, t0: float, t1: float) -> (np.ndarray, np.ndarray):

        indices = np.nonzero((t0 <= t) & (t <= t1))[0]  # select indices for the first dimension, thus [0] (see
        # numpy.nonzero documentation)

        if indices.size == 0:  # no events to show for this interval
            return np.asarray([t0, t1]), np.asarray([0, 0])

        first = indices[0]
        last = indices[-1]

        t_sel = t[first:(last+1)]
        y_sel = y[first:(last+1)]

        # np.concatenate([np.asarray([4]), a])

        if t[first] > t0 and first > 0:
            t_sel = np.concatenate([np.asarray([t0]), t_sel])
            # interpolate the first point of the waveform:
            y_interp = y[first-1] + (y[first]-y[first-1]) * (t0-t[first-1]) / (t[first]-t[first-1])
            y_sel = np.concatenate([np.asarray([y_interp]), y_sel])

        if t[last] < t1 and last < t.size-1:
            t_sel = np.concatenate([t_sel, np.asarray([t1])])
            # interpolate the last point of the waveform:
            y_interp = y[last] + (y[last+1]-y[last]) * (t1-t[last])/(t[last+1]-t[last])
            y_sel = np.concatenate([y_sel, np.asarray([y_interp])])

        return t_sel, y_sel

    @classmethod
    def remove_repeated_values(cls, t: np.ndarray, x: np.ndarray) -> (np.ndarray, np.ndarray):
        """
        Removes repeated values in array t, and returns reduced arrays t and t.
        :param t: a time vector
        :param x: a value vector
        :return: reduced vectors t and t
        """

        selection = np.ones(x.shape, dtype=bool)
        selection[1:] = (x[1:] != x[0:-1])

        return t[selection], x[selection]

    @classmethod
    def set_plot_data(cls, ax, x: np.ndarray, y: np.ndarray):

        for c in ax.get_children():
            if isinstance(c, matplotlib.lines.Line2D):
                c.set_data(x, y)
                ax.draw()
                return

    @classmethod
    def merge_time_series(cls, t1: np.ndarray, y1: np.ndarray, t2: np.ndarray, y2: np.ndarray) -> \
            Tuple[np.ndarray, np.ndarray]:
        # operates on decimated arrays

        t = np.union1d(t1, t2)  # merged time axis

        y = np.zeros(t.shape)

        t1p = 0  # pointer into t1
        y1p = 0  # current value of waveform 1
        t2p = 0  # pointer into t2
        y2p = 0  # current value of waveform 2

        for i in range(0, t.size):
            curr_t = t[i]

            if t1p < t1.size and t1[t1p] == curr_t:
                y1p = y1[t1p]
                t1p += 1

            if t2p < t2.size and t2[t2p] == curr_t:
                y2p = y2[t2p]
                t2p += 1

            y[i] = y1p + y2p

        t, y = cls.remove_repeated_values(t, y)
        return t, y


def main():
    from SequenceTiming import SequenceTiming
    from SequenceDiagram import SequenceDiagram
    file = '/home/sebastianh/imager1_bcan/virtualbox_share/SequenceSimulation/pathfinder_2020_02_19_03_59_44/SimulationProtocol_INF.dsv'
    st = SequenceTiming(file)
    s = SequenceDiagram(st, cont_update=True)
    s.plot()


if __name__ == '__main__':
    main()
