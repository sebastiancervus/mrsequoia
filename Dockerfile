
# A docker file to generate an image for using MRSequoia with jupyter notebook.
# Written by Sebastian Hirsch
# sebastian.hirsch@bccn-berlin.de

FROM jupyter/scipy-notebook:python-3.8.8

# We have to install graphviz, since it is needed by the tree visualization in SequenceBuilder
USER root
RUN sudo apt-get update
RUN sudo apt-get install -y graphviz

USER jovyan
WORKDIR /home/jovyan

COPY *.py *.png JupyterLab_example.ipynb MRiLab_example.ipynb SequenceDiagram_example.ipynb SequenceBuilder.ipynb pyproject.toml poetry.lock /home/jovyan/
COPY tmp_SiemensTools/ /home/jovyan/SiemensTools
COPY Atoms/ /home/jovyan/Atoms
COPY test/ /home/jovyan/test
COPY SeqTree /home/jovyan/SeqTree

RUN pip install poetry poetry-kernel

RUN poetry install; # poetry run python -m ipykernel install --user --name=MRSequoia

RUN echo "c.NotebookApp.password = ''" >> /home/jovyan/.jupyter/jupyter_notebook_config.py
RUN echo "c.NotebookApp.token = ''" >> /home/jovyan/.jupyter/jupyter_notebook_config.py
