from WaveformManager import WaveformManager
from DummyEncoder import DummyEncoder

class MissingWaveformManager(WaveformManager):
    """
    A class to represent a missing waveform. This manager will always return a zero waveform of the appropriate size.
    """

    def __init__(self):
        self.encoder = DummyEncoder()

    def encoder_from_file(self) -> None:
        """
        Nothing to do, so we don't do anything. This method only exists since it is declared abstract in the base class,
        so we give it some "implementation" here.
        :return: nothing. Surprised?
        """
        pass


