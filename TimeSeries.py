"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""


class TimeSeries:
    """
    TimeSeries save one or more time series for the same data source
    """

    def __init__(self):
        self.t = []
        self.values = []

    def add_data(self, t: list, v: list):
        self.t.append(t)
        self.values.append(v)

    def min(self, i: int) -> float:
        if len(self.values[i]) == 0:
            return 0
        return min(self.values[i])

    def max(self, i: int) -> float:
        if len(self.values[i]) == 0:
            return 0
        return max(self.values[i])

    def min_max(self, i: int) -> (float, float):
        return self.min(i), self.max(i)

    def get_series(self, i: int):

        return self.t[i], self.values[i]

    def get_total_time(self):

        tt = 0
        for i in range(0, len(self.t)):
            if len(self.t[i]) > 0:
                tt = max(tt, self.t[i][-1])
        return tt

    def n(self) -> int:
        return len(self.t)
