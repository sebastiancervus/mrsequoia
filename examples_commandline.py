# This file demonstrates how to use MRSequoia from a script that can be run from the command line.
# Note that graphical output is not possible in this mode. If you want to use the plotting functionality of MRSequoia,
# please use the JupyterLab interface instead.

# Run this script as
# pipenv run python examples_commandline.py

from SequenceTiming import SequenceTiming

# Instantiate an SequenceTiming object from simulation output of the standard FLASH sequence:
file = 'SiemensTools/examples/FLASH/SimulationProtocol_INF.dsv'
st = SequenceTiming(file)

# With TRCheck we can check that TR is constant by measuring the distance between successive RF pulses:
print('Running TRCheck based on the name of the first RF pulse.')
from TRCheck import TRCheck
tr_check = TRCheck(st.tx[0].name)  # the argument is used as a selector, i.e., all atoms with a name identical to
# the name of the first TX pulse are selected

tr_check.run_check(st)
print('')

# The same, but using a lambda expression as a selector. Chooses all atoms that are of type TXEvents and have a
# flip angle of 15°. The isinstance() condition is necessary, since not all classes derived from AtomBase have an
# angle property. Therefore, not checking for the type first would cause an AttributeError.
print('Running TRCheck based on atom type and flip angle.')
from Atoms.TXAtom import TXAtom
selector = lambda x: isinstance(x, TXAtom) and x.angle == 15
tr_check = TRCheck(selector)
tr_check.run_check(st)
print('')


# We can also specify the expected value of TR. The following check will fail, because the actual TR was 10,000 µs.
print('Checking if TR is 90,000 µs (spoiler: it is not)')
tr_check.run_check(st, 90000)
print('')

# VariableDelayCheck allows us to verify that the interval between two different atoms only has values from a
# specified set of allowed values. In this example, we check that the delay between an excitation pulse and a phase-
# encode rewinder gradient is 90,000 µs, 100,000 µs, or 32560 µs.
from VariableDelayCheck import VariableDelayCheck
print('Running VariableDelayCheck.')
vd_check=VariableDelayCheck('SRFExcit', 'PERew')
allowed_values = [90000, 100000, 32560]
vd_check.run_check(st, allowed_values)
print('')


# The same check will fail if we change the set of allowed values:
print('Running another VariableDelayCheck that is designed to fail:')
allowed_values = [13000]
vd_check.run_check(st, allowed_values)
print('Told you so!')
print('')

# For each ADC event, the corresponding MDH (measurement data header) is available:
print('Showing MDH information for the first ADC object')
st.adc[0].adc_info.print()
print('')

print('And that concludes our introduction to MRSequoia. Have fun exploring!')

