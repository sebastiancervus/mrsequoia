#!/bin/bash

# First, we copy the contents of the SiemensTools folder (which might be a symlink to a directory outside of docker's
# build context to a folder tmp_SiemensTools, which will then be copied to the docker image during the build process.

if [ -d tmp_SiemensTools ];
then
  rm -rf tmp_SiemensTools
fi

if [ -d SiemensTools ];
then
  rsync -Lr SiemensTools/ tmp_SiemensTools
else
  mkdir tmp_SiemensTools
fi

docker build --tag=mrsequoia .

rm -rf tmp_SiemensTools
