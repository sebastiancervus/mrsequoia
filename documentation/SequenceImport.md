## Import simulation files:
Currently, only the simulator output from Siemens IDEA is supported. See the Installation document on how to install the Siemens importer from the IDEA forum.

Read simulation output by pointing towards the *_INF.dsv file (the one containing the realtime event blocks):
```python
from SequenceTiming import SequenceTiming
file = 'SimOutput/FLASH/SimulationProtocol_INF.dsv'
st = SequenceTiming(file)
```
This imports the atoms from the simulation output and populates the AtomLists (st.all_atoms, st.tx, st.gro, ...) with Atom objects.