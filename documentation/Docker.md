### Docker support
[Docker](https://www.docker.com/) is a framework that allows one to run pre-bundled software packages in a virtual container, without the need to install Python or Jupyter. MRSequoia includes a *Dockerfile* and scripts to build a docker container and deploy it with minimal effort.
Files within the docker container are read-only, however, the folder MRSequoia_data within the docker image can be mapped to an arbitrary folder of the operating system (see `run_docker.sh`), allowing you to use your own simulation data inside the docker container.



*The following instructions were tested on Linux; further steps might be necessary on Windows or MacOS.*

1. Install docker, and make sure that the user is a member of the docker group.
2. Make the bash scripts executable: `chmod u+x run_docker.sh build_docker.sh`.
3. Build a docker container: `./build_docker.sh`. This will download all necessary dependencies and can take a couple of minutes.
4. *(Optional): Open run_docker in a text editor and change the values for data_dir_source (the directory of your local computer that will be available within the docker container), and the local port through which to access Jupyter.*
5. Run the container: `./run_docker.sh`
6. Open a browser, go to `http://127.0.0.1:local_port`, where `local_port` is the port number set in run_docker.sh (defaults to 20000)
7. Once you are logged into Jupyter, you can run the example notebook by opening MRSequoia/JupyterLab_example.ipynb. To access your own simulator output, copy it to `data_dir_source` (as specified in `run_docker.sh` on the host computer, see step 4) and access it inside the docker container under `~/MRSequoia_data`.
