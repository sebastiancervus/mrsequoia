# SequenceBuilder
SequenceBuilder allows the user to define a sequence within MRSequoia, without the need to import external sequence simulation results. The sequence can be built up using a tree concept (as e.g. used by the [JEMRIS](http://www.jemris.org) or [SequenceTree](https://github.com/magland/sequencetree4) packages).

The sequence tree is built upon the concept of executing the nodes in a left-to-right, depth-first fashion. The root of the tree represents the entire measurement. All tree nodes belong to one of several classes, all being derived from the abstract SequenceNode class:
 - A *LoopNode* represents an loop over an index, such as a phase-encode loop iterating over an k-space line index. For each execution of the loop, the corresponding index is incremented, and all children of the LoopNode are executed left-to-right, depth-first.
 - An *ActionNode* encapsulates a SequenceAtom instance, such as a gradient, an RF pulse, or an ADC. Executing an ActionNode means inserting the encapsulated Atom into the SequenceTiming structure. If two Atoms are to be performed simultaneously, such as an RF pulse accompanied by a slice-select gradient, those ActionNodes are in a parent-child relationship (i.e., the RF pulse ActionNode is a child of the slice-select gradient ActionNode).
 - A *CompoundNode* represents a sequence of Atoms that are to be executed sequentially and form a logical unit, such as a prephaser - readout gradient - rephaser sequence. The individual actions are implemented as ActionNodes containing the corresponding atoms, and are executed left-to-right.
 - A *DelayNode* simply inserts a delay of specified duration into the sequence timing.
 
 Nodes are composed into the tree structure using the `SequenceBuilder.add_child(parent, child)` method. SequenceBuilder furthermore provides convenient methods for accomplishing typical tasks, such as creating a slice-select or readout CompoundNode.
 
 To allow for exchange of information between Nodes, SequenceBuilder also implements a `SequenceMediator` object. It acts basically as a dictionary, in which information can be stored in a key/value structure. Every node can query any key for its value; however, only the owner of a key (i.e. the instance that first registered that key) can change its value to avoid interference. SequenceMediator should be the exclusive mechanism for nodes to exchange information, since any direct interaction might lead to race conditions.
 
## Simulation
 The main purpose of SequenceBuilder is to construct a SequenceTiming object that can then be exported to MRiLab for MR physics simulation. This allows us to simulate what an image acquired with a specific sequence and a set of parameters would look like in real life. ```SequenceBuilder.build_sequence()``` converts a SequenceTree into a SequenceTiming object, which can then be written to an XML file using MRiLabExporter. The resulting file can be imported into MRiLab and used for sequence simulation right away.
 
## Slice orientation and gradient axes
The default slice orientation when exporting sequences to MRiLab is transverse, with the phase-encode direction anterior-posterior. In order to scan rotated slices, three Euler angles can be set via ```SequenceBuilder.set_rotaton_angles(alpha, beta, gamma)```. The meaning of these angles is discussed below.

**Note:** MRiLab (as of v1.3) only allows for scanning coronal, sagittal and transverse/axial slices; tilted slices are not natively support. The mechanism described here can be used to scan (doubly) oblique slices at arbitrary orientations nevertheless, however, there is a significant performance penalty. By default, the spin physics simulation in MRiLab is only performed for the imaging volume (unless one or more of the ```No[Slice/Phase/Freq]Alias``` fields on the Advanced tab are set to off, in which case the simulation is extended beyond the FOV borders in that direction). This leads to longer simulation times. When the slice is tilted, MRiLab does not know about this, so the simulation volume has to be set large enough to contain the entire tilted slice (the simulation volume is always parallel to the x/y/z axes and does not get tilted) by either setting ```FOVFreq/FOVRes/SliceThick``` parameters in the MRiLab GUI (they are not used for the simulation itself) so that the resulting box contains the entire slice, or by enabling aliasing along the axes. On a laptop with an Intel i7 quadcore processor and no GPU, simulation time for a simple spin-echo sequence of a single slice increased from about 20 minutes when axial to about 7 hours when tilted.

For scanning rotated or angled slices, we use the following mechanism:
We use a human volunteer, lying head-first in supine position in the scanner as an anatomical reference.
We define the static physical coordinate system as follows:
- *x*-axis: right ear → left ear
- *y*-axis: anterior → posterior (down)
- *z*-axis: feet → head

This coordinate system is the same as the one defined by the DICOM standard.
In this reference frame, the default slice orientation is axial (=transverse), with *x* the phase-encode direction and *y* the readout direction. To achieve arbitrary slice orientations, we perform a sequence of extrinsic (i.e., defined in terms of the stationary coordinate axis) rotations using [Euler angles](https://en.wikipedia.org/wiki/Euler_angles) alpha, beta, gamma about the *z*, *x*, *z* axes (i.e. two rotations about the head-feet axis, with an intermittent rotation about the ear-ear axis). The ranges for the angles are [-180°, +180°] for alpha and gamma, and [-90°, +90°] for beta.

In MRiLab, the gradient axes are always defined relative to the slice orientation. GxRO (**x'**) is always the readout axis, GyPE (**y'**) is the (in-plane) phase-encode axis, and GzSS (**z'**) is the (through-plane) slice-select (or second phase-encode) axis.

However, as of version 1.3, MRiLab does not support scanning tilted/oblique slices. In order to do this, we use the following trick:
1. We set the slice geometry in MRiLab to transverse (axial), readout direction L/R (independent of the actual slice orientation that we want to scan)
2. In SequenceBuilder, we define a series of three rotations (see Euler angles above) that rotates this initial slice orientation to the desired one.
3. In MRiLabExporter, we distribute the three gradient axes onto the GxRO, GyPE and GzSS axes (for example for an in-plane rotation by 45°, both GxRO and GyPE would receive contributions from the readout and phase-encode gradients, weighted with ±1/sqrt(2)). In doing so, we re-define the GxRO, GyPE and GzSS axes from logical to physical axes.

We thus calculate a rotation matrix **R** that maps [x, y, z] to [x', y', z'] via

[x', y', z']^T = R * [x, y, z]^T. This matrix is stored as ```SequenceTiminig.rotation_matrix.```
## Dynamic variables
Dynamic variables are properties of Atoms (gradients, TX pulses, etc.) that change from iteration to iteration (e.g., based on a loop index) and thus have to be re-calculated before each execution of the atom. For example, the amplitude of a phase-encode gradient has to be adapted for each execution of the phase-encode loop. Dynamic variables are handled in the following way:

The ActionNode class provides a method `register_dynamic_properties` that accepts a list of tuples as an argument. Each tuples is comprised of two elements:
1. a string identifying a property of the encapsulated Atom, e.g., `amplitude`or `duration`. The property will be set using the `settattr` function of Python, meaning that also new properties can be added dynamically to the Atom. While it is in principle possible to alter the `duration` property of an Atom, this is discouraged since it will change the sequence timing from one iteration to the next
2. a `ParameterizedFunction` object that is used to calculate the value for the parameter specified above (see below for documentation on `ParameterizedFunction`)

Before each execution of an ActionNode, the registered dynamic variables are re-calculated in the order in which they are provided to `register_dynamic_properties`. This means that if you want to specify two dynamic variables, *X* and *Y*, where the (new) value of *Y* depends on the (new) value of *X*, then *X* should appear before *Y* in the list provided to `set_dynamic_properties` so that *X* is calculated first.

### ParameterizedFunction
The ParameterizedFunction class encapsulates a function together with its parameters. The latter are dynamically resolved at runtime, using either properties of the associated Atom object, or queried from the SequenceMediator. The constructor takes three arguments:
```python
fun = ParameterizedFunction(owner, fun, params_list)
```
where `owner` is an ActionNode instance for which the parameters are to be calculated. `fun` is a function reference (or lambda) with the following signature: 'List[float] -> float' that calculates the dynamic property from a set of other properties. `params_list` ist a list of strings, each of which identifying either a property of the owner.atom instance, or a lookup key for the SequenceMediator object. To illustrate the mechanism, let us consider the following simple example:

```python
fun = lambda x: x[0]*x[1]
input = ['tr_idx', 'pe_idx']
pf = ParameterizedFunction(owner, fun, input)
```    
 Now the following happens whenever pf.get_value() is invoked (this happens automatically whenever the `execute` method of the ActionNode instance is performed):
 1. The first element of `input`, 'tr_idx', is resolved to a value. To this end, first owner.atom is queried for a field `tr_idx`. If it exists, the value stored in this field is assigned to `x[0]` (assuming that the value is a float, or at least a numerical value. No type-checking is performed.) If 'tr_idx' is not a member variable of the atom instance, the SequenceMediator instance is instead queried for an entry 'tr_idx'. If it exists, its value is used for `x[0]`, otherwise a KeyError is raised.
 2. The same process is repeated for the second input argument, 'pe_idx'. If it can be resolved to a value, it will be stored in `x[1]`.
 3. Now `fun(x)` is executed and will only multiply two numbers contained in `x`. In this case, the result would be the product of the current TR and phase-encode loop indices (which probably does not make a whole lot of sense, but it illustrates the purpose).
 
 To put it all together, we demonstrate the creation of an (incomplete) sequence with a TR and an enclosed phase-encode loop and a gradient objects whose amplitude depends on the phase-encode loop index:
 ```python
sb = SequenceBuilder.SequenceBuilder()

med = sb.mediator
# prepare nodes individually
tr_loop = LoopNode.LoopNode('TR-loop', med, 'tr_idx', 3)
pe_loop = LoopNode.LoopNode('PE-Loop', med, 'pe_idx', 20)
rf = sb.tx_node('rf', 14.4, 460)
adc = sb.adc_node('adc', 30, 100)
grad = sb.gradient_node('pe-grad', 0, 500, 20, 20, AtomType.GS)

# for the gradient, we want an amplitude that depends on the phase-encode loop index
ampl_fun = ParameterizedFunction(grad, lambda x: x[0], ['pe_idx'])
grad.register_dynamic_properties([('amplitude', ampl_fun)])

sb.add_child(sb.root, tr_loop)
sb.add_child(tr_loop, pe_loop)
sb.add_child(pe_loop, grad)
sb.add_child(grad, rf, delay=20)
sb.add_child(pe_loop, adc)

sb.plot_tree()
 ```   
## Example: Balanced gradient echo
First, we import the necessary packages:

```python 
from SeqTree.SequenceBuilder import SequenceBuilder
from SeqTree.LoopNode import LoopNode
from SeqTree.DelayNode import DelayNode
from Atoms.TXAtom import TXAtom
from SeqTree.PhaseEncoderNode import PhaseEncoderNode
from SeqTree.CompoundNode import CompoundNode
```
Next, we instantiate a SequenceBuilder object and define some acquisition parameters:

```python    
sb = SequenceBuilder(max_amplitude=40, max_slewrate=200e-3)
med = sb.mediator

n_pe = 64  # phase-encode lines
n_ro = 64  # read-out columns
fov_ro = 0.256  # field-of-view in readout direction
fov_pe = 0.256  # same for phase-encode
```
Now we instantiate various nodes of the sequence tree:
    
```python
# Phase-encode loop with a duration of 1 sec per repetition
pe_loop = LoopNode.pe_loop(med, n_pe, duration=1e6)

# Phase-encode node with duration 500 µs and 100 µs ramp time
# The encapsulated gradient adapts it amplitude based on the index 'pe_idx',
# which is the default index of a loop created using LoopNode.pe_loop
pe_grad = PhaseEncoderNode('PE-grad', med, 500, fov_pe, n_pe, 100, 100, 'pe_idx')
    
# Phase-rewinder node. This is exactly the same as the phase-encoder node, but with inverted polarity.
pe_rev_grad = PhaseEncoderNode('PE-rewinder', med, 500, fov_pe, n_pe, 100, 100, 'pe_idx', rewinder=True)
    
# A sinc excitation pulse with a flip angle of 90° and a duration of 5120 µs
tx_atom = TXAtom.sinc_pulse(0, 90, 5120, 0)
# Turn the excitation pulse into an excitation node by adding a simulateneous
# slice-select gradient (plus pre/rephasers) for a slice thickness of 5 mm
exc_comp = sb.excitation_node('Excitation', tx_atom, 5e-3)
    
# The readout complex, consisting of prephaser/reaout/rephaser gradients,
# and an ADC. The readout time (plateau time of the ro gradient) is 500 µs,
# and the ramp time is 300 µs.
ro_comp = sb.readout_node('Readout_comp', fov_ro, n_ro, 500, 300)
```
Now let's assemble the tree and visualize it:
```python
st, simu_params = sb.build_sequence()
# st is the SequenceTiminig object generated from the tree. simu_params is a dictionary
# containing the simulation parameters that will be written to SimuAttr.xml to inform
# MRiLab about sequence parameters.
    
sb.plot_tree()
```
Output:
[![](Gradient_Echo_Tree_View.png)](Gradient_Echo_Tree_View.png)
(Click to enlarge image)
The shapes of the tree node indicate the type of the node (loop, gradient, RF pulse, ADC, compound, delay)
The color of the gradients indicates the logical axis (SS, PE, RO) on which the gradient is played out.


Let's take a look at the sequence timing diagram.

```python
%matplotlib widget
from SequenceDiagram import SequenceDiagram
    
sd = SequenceDiagram(st)
sd.plot()
```
Zoomed display of the first execution of the PE-loop:
[![](Gradient_Echo_Sequence_Diagram.png)](Gradient_Echo_Sequence_Diagram.png)
(Click to enlarge image)

### Export to MRiLab:
```python
from MRiLabExporter import MRiLabExporter
from pathlib import Path

ex = MRiLabExporter(st, 100)
outpath = str(Path.home()) + '/tmp/ge_test'
ex.write_mrilab_xml(outpath,'PSD_ge_test.xml', simu_params)
```
The sequence has now been written to ```tmp/ge_test``` in the user's home directory.
**Important**: The name of the XML file must start with 'PSD_', otherwise it will not show up in MRiLab's import interface.   
 
After loading the sequence into MRiLab and running the simulation on the standard brain phantom, the output should look similar to this:
[![](MRiLab_Gradient_Echo_Example.png)](MRiLab_Gradient_Echo_Example.png)
(Click to enlarge image)

See [MRiLab.md](MRiLab.md) for details on how to run the simulation.
 
## Notes
- The dynamic properties are updated in each iteration right before an ActionNode is executed. Since the entire sequence tree is processed top to bottom, this means that children can access their parents' updated properties, but not vice versa.

## References
- Stöcker T, Vahedipour K, Pflugfelder D, & Shah NJ. High-performance computing MRI simulations. Magnetic Resonance in Medicine, 2010, 64(1). https://doi.org/10.1002/mrm.22406
- Magland JF, Li C, Langham MC, Wehrli FW. Pulse sequence programming in a dynamic visual environment: SequenceTree. Magnetic Resonance in Medicine, 2016, 75(1). https://doi.org/10.1002/mrm.25640

