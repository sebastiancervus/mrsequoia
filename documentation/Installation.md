# Installation

MRSequoia is platform independent and should run on Windows, Linux and MacOS. Only Linux compatibility has been tested, though, so unexpected things *might* happen on other operating systems. The following instructions are as generic as possible, however, additional OS-specific steps might be necessary.

As an alternative to a local installation, MRSequoia also includes a docker file to build a virtual container to deploy MRSequoia without the need to install Python packages locally. See [Docker.md](Docker.md) for details.

1. Make sure that Python is installed. MRSequoia was developed on Python 3.8, and backward compatibility has not been tested, so the safest option is to use Python 3.8.
2. Install poetry (dependency manager for Python) - see [Poetry documentation](https://python-poetry.org/docs/#installation).
3. Install MRSequoia by unzipping the archive file to an arbitrary location. Also install the Siemens importer (see below).
4. Open a shell and `cd` to the MRSequoia folder. (Note: when running on Windows with Anaconda, make sure that you use a Python terminal, not the generic Windows cmd terminal, since otherwise the necessary paths will not be set.)

By default, a Python virtual environment is used via Poetry to manage all library dependencies and to avoid clashes with other Python projects. The following steps will set up such a virtual environment. You can use Pipenv instead, which is still included for legacy reasons (see below under "Legacy pipenv support")

4. Install all necessary python libraries by typing `poetry install` on the command line. (Hint: you might have to add the location of the pipenv executable to your PATH variable.)

5. If you want to run a python script, use one of the following two options. However, running a script like this will not produce graphical output, so if you want to make use of the plotting functionality, use the JupyterLab method below.
 * run `poetry shell` to activate a shell within the virtual environment, then run `python <script_file.py>` to run the script.
 * run `poetry run python <script_file.py>` to run the script directly from outside the virtual environment.
 * A usage example can be found in examples_commandline.py
 * Alternatively, MRSequoia can be run interactively in a browser section by using JupyterLab (see below).
 
## Legacy pipenv support
* Rename the files _Pipenv and _Pipenv.lock and remove the trailing underscore.
* Install all necessary python libraries by typing `pipenv install` on the command line. (Hint: you might have to add the location of the poetry executable to your PATH variable.)
* run `pipenv shell` to activate a shell within the virtual environment, then run `python <script_file.py>` to run the script.
* run `pipenv run python <script_file.py>` to run the script directly from outside the virtual environment.
**Note:** The pipenv dependencies are not updated any more, so it might be necessary to install additional dependencies via "pipenv install <package>". To find the proper package names, see pyproject.toml.

## Importer for Siemens IDEA
To import the proprietary simulation files produced by the Siemens IDEA simulator, additional code is required that can be downloaded from the Siemens IDEA forum (www.magnetom.net). After logging in, search for a discussion titled "Introducing MRSequoia", and look for the newest post with a download link. After downloading the zip file, unpack it into the MRSequoia installation directory, so that there is a SiemensTools folder within the MRSequoia directory.

**Please note that distribution of the MRSequoia_SiemensTools package is only permitted via the magnetom.net website. Distributing the code contained therein via any other channels, or uploading it to public repositories, is strictly prohibited.**


 
## JupyterLab support
[JupyterLab](https://jupyter.org/) allows you to run Python code interactively in a browser session. This is the preferred mode if you want to make use of MRSequoia's plotting functionality. You can either install jupyter globally (so that it can be used for other projects as well), or into the MRSequoia virtual environment. A short summary of the necessary steps is given below, please consult the [JupyterLab installation page](https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html) for more information:
   
* For a global installation, either use the package manager of your operating system to install the JupyterLab package, or run the package manager of your python distribution: `pip install jupyterlab`. To install the Poetry kernel for jupyter (necessary for running the MRSequoia virtual environment inside Jupyter), run `pip install --user poetry-kernel`. You can start Jupyter Lab from the command line by typing `jupyter lab`.

* For a local installation into the MRSequoia virtual environment, `cd` to the MRSequoia directory and run `poetry add jupyterlab poetry-kernel`. After that, you can start Jupyter Lab from the MRSequoia directory by typing `poetry run jupyter lab`

In Jupyter, select the Poetry kernel for new notebooks. This will automatically activate the virtual environment for MRSequoia, as long as the notebooks are stored inside the MRSequoia folder (or a subfolder thereof).

---
**Note:**
In Windows, poetry-kernel does not seem to be working at the moment for the local Jupyter installation method. If Jupyterlab keeps showing "Connecting", and displays an error message "FileNotFoundError: [WinError 2] The system cannot find the file specified",
you can instead install a non-poetry kernel. In a command shell, go to the MRSequoia directory, and type `poetry run python -m ipykernel install --name MRSequoia`, then start jupyter lab. In the menu bar, click on Kernel->Change Kernel and select "MRSequoia".
---

---
**Note:**
If a plotting command in Jupyter Lab results in "Error displaying widget: model not found", you have to install the Jupyter Lab matplotlib extension. Open a command line in the MRSequoia directory, and type

`jupyter labextension install jupyter-matplotlib` (if you used the global installation method for Jupyter Lab) or
`poetry run jupyter labextension install jupyter-matplotlib` for the local method. After restart Jupyter Lab, plotting should work.
---

### Jupyter kernel for legacy pipenv:
1.  In the MRSequoia directory, first activate the virtual environment: `pipenv shell`
2.  Run `python -m ipykernel install --user --name=MRSequoia`. This will install a new jupyter kernel called MRSequoia. You can select the kernel for each Jupyter notebook by selecting "Kernel -> Change kernel" from the menu of the notebook, or by clicking on the kernel name in the top right corner of the notebook. (Note: if an instance of jupyterlab is already running, you might have to restart it in order for the newly installed kernel to be selectable).
3.  From the command line, start the Jupyter server: `jupyter lab` (or, if JupyterLab is already running, restart it).
4.  This should open a browser tab with the JupyterLab interface. Navigate to the MRSequoia folder and either open an existing notebook or create a new one. In either case, make sure that the MRSequoia kernel is selected (Kernel->Change kernel or check the top right of the notebook).
5.  Use the JupyterLab_example.ipynb notebook as a reference.

**Windows:** In order to run JupyterLab on Windows, you might have to install the pywin32 package: `pipenv install pywin32`

If a plot command in JupyterLab only displays a text output similar to this:
```
VBox(children=(HBox(children=(Button(description='<-', style=ButtonStyle()), FloatSlider(value=0.5, descriptio…
Output()
```
there might be a mismatch between the JupyterLab version and the jupyterlab-manager extension. See (https://github.com/jupyter-widgets/ipywidgets/tree/master/packages/jupyterlab-manager) for a list of compatible versions (and installation instructions).