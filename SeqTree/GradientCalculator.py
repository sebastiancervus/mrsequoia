"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from MRConstants import gamma_mT_µs
from typing import Tuple
from math import sqrt, pi, ceil


class GradientCalculator:
    """
    A class with functions for calculating different gradient properties
    The following units *must* be used:
    - for magnetic fields: mT
    - for durations: µs
    - for length: m
    """

    # pre-defined gradient performance parameters (amplitude, slewrate)
    gradient_parameters: dict = {'Siemens_Prisma': (80, 200e-3)}
    default_scanner: str = 'Siemens_Prisma'

    def __init__(self, max_ampl: float = 0, max_slewrate: float = 0):
        """
        The default constructor :param max_slewrate: the maximum slewrate of the gradient system. This will be used
        for all gradient ramps whenever the ramp time is not specified explicitly
        """
        self.max_ampl: float = max_ampl or self.gradient_parameters[self.default_scanner][0]  # maximum gradient amplitude in mT/m
        self.max_slewrate: float = max_slewrate or self.gradient_parameters[self.default_scanner][1]  # maximum gradient slewrate in mT/(m*µs)

    def calculate_gradient_area(self, amplitude: float, duration: int, rut: float = 0, rdt: float = 0) -> float:
        """
        Calculate the area under the trapezoidal gradient
        :param amplitude: the gradient amplitude, in  mT/m
        :param duration: the duration, in µs (start - end of plateau)
        :param rut: ramp-up time, in µs
        :param rdt: ramp-down time, in µs
        :return: the area under the gradient, in mT/m * µs
        """
        rut, rdt = self.get_ramp_times(amplitude, rut, rdt)
        return amplitude * (duration - rut / 2 + rdt / 2)

    def calculate_area_under_ramp(self, amplitude: float, ramp_time: int) -> float:
        """
        Calculate the area under a gradient ramp
        :param amplitude: the gradient amplitude, in mT/m
        :param ramp_time: the ramp-time, in µs
        :return: the area under the ramp, in mT/m * µs
        """

        ramp_time, _ = self.get_ramp_times(amplitude, ramp_time, ramp_time)

        return amplitude * ramp_time / 2.0

    def timing_from_moment(self, m: float, sr: float = 0, max_ampl: float = 0) -> (float, float, float):
        """
        Calculate the duration and ramp-down time of a gradient with the specified moment, and slew rate. If slew rate
        is not specified, the maximum slew rate of the gradient system is used. It is first checked whether a triangular
        gradient up to the maximum available gradient amplitude is sufficient. If not, the gradient is ramped up to the
        maximum and a plateau of the appropriate duration is inserted. The resulting gradient will have identical
        rise/fall times for the two slopes
        :param m: moment, in mT/m * µs
        :param sr: slew rate, in mT/m/µs. If 0 or not specified, use maximum available slew rate
        :param max_ampl: maximum gradient amplitude to use, in mT/m. If 0 or not specified, use maximum value allowed by the gradient system
        :return: a tuple (amplitude, duration, ramp-time)
        """

        # TODO: Test this function:

        if sr > self.max_slewrate:
            raise ValueError(f'Given slew rate ({sr} is larger than the maximum slew rate of the system ({self.max_slewrate}).')
        if sr == 0:
            sr = self.max_slewrate
        if max_ampl > self.max_ampl:
            raise ValueError(
                f'Given gradient amplitude ({max_ampl} is larger than the maximum amplitude of the system ({self.max_ampl}).')
        if max_ampl == 0:
            max_ampl = self.max_ampl

        max_ampl = abs(max_ampl)  # just in case the amplitude was specified as negative. We do calculations with
        # positive amplitude here and adjust the sign later, if necessary.

        if m == 0:  # if there is no moment, we don't need a gradient
            return 0, 0, 0

        ampl = min(max_ampl, sqrt(abs(m)*sr))

        rut = ampl / sr
        duration = abs(m) / ampl

        # round ramp times and duration to integer µs values
        rut = ceil(rut)
        duration = ceil(duration)

        # adapt amplitude so for the rounded durations:
        ampl = m / duration


        #ampl *= m/abs(m)  # correct the sign of the amplitude to that of the moment
        return ampl, duration, rut

    def calculate_readout_gradients(self, fov: float, n: int, plateau_time: float, ramp_time: float = 0) -> (
            Tuple[float, float, float], Tuple[float, float, float], Tuple[float, float, float]):
        """
        Calculate the prephaser, readout and rephaser gradients for a readout. We use the assumption that the
        baseline is the first line of the right half of k-space, if n is even. The (p)rephaser gradients have the
        same amplitude as the main gradient, which is calculated from the resolution and duration parameters
        :param fov: the size of the FOV in readout direction, in m
        :param n: the number of sampling points
        :param plateau_time: the  duration of the plateau time of the readout gradient (ramp + plateau), in µs
        :param ramp_time: ramp time of the readout gradient and the (p)rephaser gradients, in µs
        :return: A tuple of three tuples (amplitude, duration, ramp-time) for the prephaser, readout gradient, rephaser
        """
        deltak = 1 / fov
        kmax = (n-1)/2*deltak

        ro_ampl = 2*pi*kmax/(gamma_mT_µs*plateau_time) * 2  # the gradient has to go from -kmax to +kmax, hence *2
        ramp_time, _ = self.get_ramp_times(ro_ampl, ramp_time, ramp_time)

        moment_under_ramp = ramp_time * ro_ampl / 2.0

        m_ro = 2*moment_under_ramp + plateau_time*ro_ampl

        # prephaser moment:
        time_to_zero = plateau_time/2 + plateau_time/n/2
        m_pp = -(ro_ampl*time_to_zero + moment_under_ramp)

        pp_ampl, pp_duration, pp_rut = self.timing_from_moment(m_pp, sr=0.02)

        # rephaser moment:
        m_rp = -(m_ro + m_pp)  # m_pp + m_ro + m_rp = 0
        rp_ampl, rp_duration, rp_rut = self.timing_from_moment(m_rp, sr=0.02)

        pp = (pp_ampl, pp_duration, pp_rut)
        ro = (ro_ampl, ramp_time+plateau_time, ramp_time)
        rp = (rp_ampl, rp_duration, rp_rut)

        return pp, ro, rp

    def check_slewrate(self, amplitude: float, ramp_time: float) -> bool:
        """
        Ensure that the given ramp time and amplitude are possible with the maximum gradient performance
        :param amplitude: desired amplitude, in mT/m
        :param ramp_time: desired ramp-time, in µs
        :return:
        """
        if abs(amplitude / ramp_time) > self.max_slewrate:
            sr = amplitude / ramp_time
            raise ValueError(
                f'Slewrate {sr} with amplitude {amplitude} mT/m and ramp-time {ramp_time} µs exceeds the maximum '
                f'slewrate of {self.max_slewrate} mT/m/µs')
        return True

    def get_ramp_times(self, amplitude: float, rut, rdt) -> (float, float):
        """
        Set the ramp times to the minimum possible with the specified slewrate, unless ramp times are stated explicitly
        :param amplitude: gradient amplitude, in mT/m/µs
        :param rut: ramp-up time, in µs
        :param rdt: ramp-down time, in µs
        :return: ramp-up and ramp-down times
        """

        if rut <= 0:
            rut = abs(amplitude / self.max_slewrate)
        else:
            self.check_slewrate(amplitude, rut)
        if rdt <= 0:
            rdt = abs(amplitude / self.max_slewrate)
        else:
            self.check_slewrate(amplitude, rdt)

        return rut, rdt


def main():
    from SeqTree.SequenceBuilder import SequenceBuilder
    from SeqTree.LoopNode import LoopNode
    from SeqTree.ParameterizedFunction import ParameterizedFunction
    from SeqTree.PhaseEncoderNode import PhaseEncoderNode
    from Atoms.TXAtom import TXAtom
    from Atoms.AtomType import AtomType

    sb = SequenceBuilder()

    med = sb.mediator
    tr_loop = LoopNode('TR-loop', med, 'tr_idx', 1)

    n_pe = 10
    # pe_loop = LoopNode.LoopNode('PE-Loop', med, 'pe_idx', n_pe)
    pe_loop = LoopNode.pe_loop(med, n_pe)
    ss_grad = sb.gradient_node('SS-grad', 10, 1000, 300, 300, AtomType.GS)
    ampl_fun = ParameterizedFunction(ss_grad, lambda x: x[0], ['pe_idx'])
    # ss_grad.register_dynamic_properties([('amplitude', ampl_fun)])

    pe_grad = PhaseEncoderNode('PE-grad', med, 10000, 0.128, n_pe, 500, 500, 'pe_idx')
    pe_rev_grad = PhaseEncoderNode('PE-rewinder', med, 10000, 0.128, n_pe, 500, 500, 'pe_idx', rewinder=True)

    # rf = sb.tx_node('Excitation RF', 14.4, ss_grad.atom.duration-ss_grad.atom.rut)
    tx_atom = TXAtom.sinc_pulse(0, 30, 5120, 0, 100)
    exc_comp = sb.excitation_node('Excitation', tx_atom, 5e-3)

    ro_comp = sb.readout_node('Readout_comp', 0.1, 10, 500, 20)

    sb.add_child(sb.root, tr_loop)
    sb.add_child(tr_loop, pe_loop)
    # sb.add_child(pe_loop, ss_grad)
    sb.add_child(pe_loop, exc_comp)
    # sb.add_child(grad, rf, delay=20)
    sb.add_child(pe_loop, pe_grad)
    # sb.add_child(ss_grad, rf, delay=ss_grad.atom.rut)
    # sb.add_child(ro_comp.children[1], adc)
    sb.add_child(pe_loop, ro_comp)
    sb.add_child(pe_loop, pe_rev_grad)

    sb.plot_tree()
    st = sb.build_sequence()


if __name__ == '__main__':
    main()
