"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from SeqTree.SequenceNode import SequenceNode
from SeqTree.ActionNode import ActionNode
from SeqTree.LoopNode import LoopNode
from SeqTree.CompoundNode import CompoundNode
from SeqTree.SequenceMediator import SequenceMediator
from SeqTree.DelayNode import DelayNode
from SeqTree.ParameterizedFunction import ParameterizedFunction
from anytree import RenderTree
from anytree.render import ContStyle
from anytree.exporter import UniqueDotExporter
from Atoms.TXAtom import TXAtom
from Atoms.GradientAtom import GradientAtom
from Atoms.ADCAtom import ADCAtom
from Atoms.FreqPhaseAtom import FreqPhaseAtom
from Atoms.AtomType import AtomType
from Atoms.SyncAtom import SyncAtom
from SequenceTiming import SequenceTiming
from SeqTree.GradientCalculator import GradientCalculator

from MRConstants import gamma_mT_µs, gamma_bar
from math import pi
import numpy as np

import tempfile
import os
from copy import deepcopy

from IPython.display import Image, display

from typing import Tuple, List, Union


class SequenceBuilder:
    min_grad_delay_us: float = 0.01

    def __init__(self, max_amplitude: float = 0, max_slewrate: float = 0):
        """
        The default constructor. Takes gradient system performance limits as optional limits. If not specified,
        the default values defined in GradientCalculator are used.
        :param max_amplitude: maximum gradient amplitude, in mT/m
        :param max_slewrate: maximum gradient slewrate, in mT/m/ms
        """
        self.mediator: SequenceMediator = SequenceMediator()
        self.root: LoopNode = LoopNode('Root', self.mediator, 'root_idx', 1)
        self.grad_calc: GradientCalculator = GradientCalculator(max_ampl=max_amplitude, max_slewrate=max_slewrate)
        self.mediator.set_or_update(self, 'grad_calc', self.grad_calc)
        self.rotation_angles: Tuple[float, float, float] = (0, 0, 0)

    def add_child(self, parent: SequenceNode, child: SequenceNode, delay: int = 0) -> None:
        """
        Add a child node to a parent node. Children are added left-to-right
        :param parent: the parent node
        :param child: the child to add
        :param delay: insert a delay before the child, in µs.
        :return: nothing
        """

        children = list(parent.children) if parent.children is not None else []
        if delay > 0:
            children += [DelayNode('delay', self.mediator, delay), child]
        elif delay == 0:
            children += [child]
        else:
            raise ValueError('Only positive delays are permitted. Given value: ' + str(delay))

        parent.children = children

    def print_tree(self):
        """
        Print the sequence tree in text mode
        :return: nothing
        """
        print(RenderTree(self.root, style=ContStyle()))

    def plot_tree(self):
        """
        Plot the tree by writing it to a png file and then displaying the file in Jupyter
        :return: nothing
        """

        with tempfile.TemporaryDirectory() as tmpdir:
            filename = os.path.join(tmpdir, 'graph_output.png')
            ex = UniqueDotExporter(self.root, nodeattrfunc=self.node_attr_fun)
            ex.to_picture(filename)

            pil_img = Image(filename=filename)
            display(pil_img)

    def iterate_tree(self, start_node: SequenceNode = None) -> None:
        """
        Iterate the sequence tree top to bottom and execute the nodes on the way
        :param start_node: where to start. If omitted, the root node will be used
        :return: nothing
        """

        start_node = start_node if start_node is not None else self.root
        start_node.run()

    def gradient_node(self, name: str, ampl: float, duration: int, rut: int, rdt: int, ax: AtomType) -> ActionNode:
        """
        Generate a gradient atom encapsulated in an ActionNode instance
        :param name: The name of the node
        :param ampl: gradient amplitude, in mT/m
        :param duration: gradient duration (ramp-up until end of plateau), in µs
        :param rut: ramp-up time in µs
        :param rdt: ramp-down time in µs
        :param ax: the axis on which to perform the gradient
        :return: an ActionNode enxcapsulating the gradient
        """
        atom = GradientAtom(0, name, ampl, duration, rut, rdt, ax, None)
        node = ActionNode(name, self.mediator, atom)
        return node

    def tx_node(self, name: str, angle: float, duration: int) -> ActionNode:
        """
        Generate TXAtom encapsulated in an ActionNode instance
        :param name: the name of the node
        :param angle: the flip angle, in degrees
        :param duration: the pulse duration, in µs
        :return: an ActionNode encapsulating the RF pulse
        """
        atom = TXAtom.rect_pulse(0, angle, duration, 0)
        atom.name = name
        node = ActionNode(name, self.mediator, atom)
        return node

    def adc_node(self, name: str, duration: float, num_adcs: int) -> ActionNode:
        """
        Generate an ADCAtom encapsulated in an ActionNode instance
        :param name: the name of the node
        :param duration: duration of the ADC, in µs
        :param num_adcs: the number of data points to acquire
        :return: an ActionNode encapsulating the ADC
        """
        atom = ADCAtom(0, name, duration, num_adcs, None)
        node = ActionNode(name, self.mediator, atom)
        return node

    def freq_phase_node(self, name: str, freq: float, phase: float) -> ActionNode:
        """
        Generate a FreqPhaseAtom encapsulated in an ActionNode instance
        :param name: the name of the node
        :param freq: the frequency offset, in Hz
        :param phase: the phase, in degrees
        :return: an ActionNode encapsulating the atom
        """
        atom = FreqPhaseAtom(0, name, freq, phase, None)
        node = ActionNode(name, self.mediator, atom)
        return node

    def sync_node(self, name: str, duration: int, channel: str) -> ActionNode:
        """
        Generate a FreqPhaseAtom encapsulated in an ActionNode instance
        :param name: the name of the node
        :param duration: the duration of the sync pulse, in µs
        :param channel: the channel on which to send the pulse (integer)
        :return: an ActionNode encapsulating the sync pulse
        """
        atom = SyncAtom(0, name, duration, channel, None)
        node = ActionNode(name, self.mediator, atom)
        return node

    def readout_node(self, name: str, fov: float, n: int, plateau_time: float, ramp_time: float) -> CompoundNode:
        """
        Generate a CompoundNode that encapsulates a readout, i.e., prephaser/readout/rephaser gradients and a matching
        ADC
        :param name: the name of the node
        :param fov: the field of view in readout direction, in m
        :param n: the number of sample points
        :param plateau_time: the duration of the plateau time of the readout gradient, i.e., the readout time, in µs
        :param ramp_time: the duration of the readout gradient ramps, in µs
        :return: a CompoundNode encapsulating a readout
        """

        pp_params, ro_params, rp_params = self.grad_calc.calculate_readout_gradients(fov, n, plateau_time, ramp_time)

        ro_grad = self.gradient_node('RO gradient', ro_params[0], ro_params[1], ro_params[2], ro_params[2], AtomType.GR)
        pp_grad = self.gradient_node('RO prephaser', pp_params[0], pp_params[1], pp_params[2], pp_params[2], AtomType.GR)
        rp_grad = self.gradient_node('RO rephaser', rp_params[0], rp_params[1], rp_params[2], rp_params[2], AtomType.GR)

        ro_plateau_duration = ro_params[1]-ro_params[2]
        adc = self.adc_node('ADC', ro_plateau_duration, n)

        del1 = DelayNode('RO delay 1', self.mediator, self.min_grad_delay_us)
        del2 = DelayNode('RO delay 2', self.mediator, self.min_grad_delay_us)

        node = CompoundNode(name, self.mediator)
        self.add_child(node, pp_grad)
        self.add_child(node, del1)
        self.add_child(node, ro_grad)
        self.add_child(node, del2)
        self.add_child(node, rp_grad)
        self.add_child(ro_grad, adc, delay=ro_params[2])

        ro_bw = 1/2* fov * ro_grad.atom.amplitude*1e-3 * gamma_bar  # readout bandwidth in Hz (gamma_bar is in Hz/T)

        self.mediator.set_simulation_attribute('BandWidth', ro_bw)
        self.mediator.set_simulation_attribute('FOVFreq', fov)
        self.mediator.set_simulation_attribute('ResFreq', n)

        return node

    def excitation_node(self, name: str, pulse: TXAtom, thickness: float, ramp_time: float = 100) -> CompoundNode:
        """
        Produce a CompoundNode containing a slice-select gradient accompanied by an RF pulse, and a rephaser pulse
        :param name: The name of the pulse
        :param pulse: The RF pulse atom
        :param thickness: the desired slice thickness, in m
        :param ramp_time: ramp time in µs
        :return: a CompoundNode with prephaser, slicesel and rewinder gradients
        """
        if not hasattr(pulse, 'bw_Hz'):
            raise ValueError('Cannot create a slice-select gradient for a TX pulse with unknown bandwidth (pulse.bw_Hz does not exist')

        amplitude = 2*pi*pulse.bw_Hz*1e-6/(gamma_mT_µs*thickness)  # 1e6 is for scaling from Hz to MHz, to be compatible with time in µs

        slcsel = self.gradient_node('Slice-sel grad', amplitude, ramp_time+pulse.duration, ramp_time, ramp_time, AtomType.GS)
        tx_node = ActionNode('TX pulse', self.mediator, pulse)

        half_moment = slcsel.atom.get_moment(0) / 2.0
        rwd_ampl, rwd_dur, rwd_ramp = self.grad_calc.timing_from_moment(-half_moment, sr=0)
        rwd_node = self.gradient_node('SS rewinder', rwd_ampl, rwd_dur, rwd_ramp, rwd_ramp, AtomType.GS)
        prep_node = self.gradient_node('SS prephaser', rwd_ampl, rwd_dur, rwd_ramp, rwd_ramp, AtomType.GS)

        delay1 = DelayNode('SS delay 1', self.mediator, self.min_grad_delay_us)
        delay2 = DelayNode('SS delay 2', self.mediator, self.min_grad_delay_us)

        self.add_child(slcsel, tx_node, delay=ramp_time)
        comp = CompoundNode(name, self.mediator)
        self.add_child(comp, prep_node)
        self.add_child(comp, delay1)
        self.add_child(comp, slcsel)
        self.add_child(comp, delay2)
        self.add_child(comp, rwd_node)
        self.mediator.set_simulation_attribute('SliceThick', thickness)
        self.mediator.set_simulation_attribute('FlipAngle', pulse.angle)

        return comp

    def build_sequence(self) -> (SequenceTiming, dict):
        """
        Build a sequence (i.e., a SequenceTiming event) from a sequence tree. Additionally, the parameters necessary for
        generating the XML code for the SimuAttr.xml file (for MRiLab) is produced.
        :return: a tuple consisting of the SequenceTiming instance and the the simulation attributes as a dict
        """
        self.mediator.clear_timing()
        self.root.insert_filltime()
        self.root.run()
        st: SequenceTiming = self.mediator.get_value('sequence_timing')

        # attach FreqPhaseAtoms for the ADCs to the respective list of SequenceTiming
        st.fre += [a.get_freq_phase_atom() for a in st.adc]

        st.waveforms_from_list()
        simu_params = self.generate_simu_dict()

        st.rot_matrix = self.rotation_matrix()

        return st, simu_params

    def node_attr_fun(self, node: SequenceNode) -> str:
        """
        Generate the text to format a node using graphviz
        :param node: the node to format
        :return: the graphviz code to format the node
        """

        add_info = node.get_additional_info()

        add_str = f'<FONT POINT-SIZE="12">{add_info}</FONT>'

        label_text = f'<<B>{node.name}</B><BR/>{add_str}>'

        style_and_shape = node.get_style_and_shape()

        return f'label={label_text}, {style_and_shape}'

    def generate_simu_dict(self) -> dict:
        """
        Produce the dictionary with simulation parameters that can be used to generate the SimuAttr.xml file used by
        MRiLab
        :return: A dict with the relevant simulation parameters
        """

        # TODO: Not all parameters are set automatically yet.

        simu_attr = self.mediator.get_value('simu_attr')

        params = dict()

        # get relevant values, set defaults if values are not present:
        params['bandwidth'] = simu_attr.get('BandWidth', 0)  # has no significance for simulation, as far as I know
        params['fov_freq'] = simu_attr.get('FOVFreq', 256e-3)
        params['fov_phase'] = simu_attr.get('FOVPhase', 256e-3)
        params['flip_angle'] = simu_attr.get('FlipAngle', 0)

        freq_dir_num = 2  # we default to L/R
        freq_dir = simu_attr.get('FreqDir', 'L/R')
        if freq_dir == 'A/P':
            freq_dir_num = 1
        elif freq_dir == 'S/I':
            freq_dir_num = 3
        params['freq_dir'] = f"${freq_dir_num}'A/P','L/R','S/I'"

        params['res_phase'] = simu_attr.get('ResPhase', 128)
        params['res_freq'] = simu_attr.get('ResFreq', 128)

        scan_plane_num = 1  # we default to axial planes
        scan_plane = simu_attr.get('ScanPlane', 'Axial')
        if scan_plane == 'Sagittal':
            scan_plane_num = 2
        elif scan_plane == 'Coronal':
            scan_plane_num = 3
        params['scan_plane'] = f"${scan_plane_num}'Axial','Sagittal','Coronal'"

        params['slice_num'] = simu_attr.get('SliceNum', 1)
        params['slice_thick'] = simu_attr.get('SliceThick', 5e-3)
        params['te'] = simu_attr.get('TE', 1)  # doesn't really matter for the simulation, as far as I know...
        params['tr'] = self.root.get_duration_of_children()/1e6  # tr is specified in seconds

        return params

    def set_rotation_angles(self, alpha: float = 0, beta: float = 0, gamma: float = 0) -> None:
        """
        Set the rotation angles for the transformation mapping physical coordinates (X, Y, Z) to logical (rotated) coordinates (x, y, z).
        The rotation order follows extrinsic Euler angle convention: First rotation by alpha about Z, then rotation by
        beta about X, then another rotation by gamma again about Z.
        :param alpha: first rotation angle (Z axis), in degree
        :param beta: second rotation angle (X axis), in degree
        :param gamma: third rotation angle (Z axis), in degree
        :return: nothing
        """
        self.rotation_angles = (alpha, beta, gamma)

    def rotation_matrix(self) -> np.ndarray:
        """
        Calculate the matrix for mapping physical coordinates (X, Y, Z) to logical (rotated) coordinates (x, y, z).
        The rotation order follows extrinsic Euler angle convention: First rotation by alpha about Z, then rotation by
        beta about X, then another rotation by gamma again about Z.

        :return: a rotation matrix R, with [x, y, z]^T = R * [X, Y, Z]^T
        """
        # rotation order: first rotation about Z by alpha, then about X by beta, then about Z again by gamma
        alpha = self.rotation_angles[0] / 360 * 2 * pi
        beta = self.rotation_angles[1] / 360 * 2 * pi
        gamma = self.rotation_angles[2] / 360 * 2 * pi

        # Matrix for case Z1X2Z3 (proper Euler angles) from https://en.wikipedia.org/wiki/Euler_angles#Conventions_by_extrinsic_rotations
        # Note that the order of application is s3, c3 first, then s2, c2, then s1, c1. Therefore we identify s3/c3 -> alpha, s2/c2 -> beta, s1/c1 -> gamma
        c1 = np.cos(gamma)
        c2 = np.cos(beta)
        c3 = np.cos(alpha)
        s1 = np.sin(gamma)
        s2 = np.sin(beta)
        s3 = np.sin(alpha)
        rot_matrix = np.array([[c1 * c3 - c2 * s1 * s3, -c1 * s3 - c2 * c3 * s1, s1 * s2],
                               [c3 * s1 + c1 * c2 * s3, c1 * c2 * c3 - s1 * s3, -c1 * s2],
                               [s2 * s3, c3 * s2, c2]])
        return rot_matrix

    @staticmethod
    def rf_spoiling_phase_cycling(idx: Union[str, List[str]], increment: float, nodes: List[SequenceNode]) -> None:
        """
        Add RF spoiling support via phase cycling to the specified atoms. Implemented according to Bernstein et al.,
        The Handbook of MRI pulse sequence, pg 582.
        :param idx: The loop index (as a string) based on which to implement the phase cycling
        (i.e., 'pe_idx' to increment for each phase-encode loop). Multiple indices can be supplied as a list of strings,
        in that case, the sum of all indices will be used to calculate the RF phase (e.g., to guarantee a smooth
        transition between prep scans and imaging scans)
        :param increment: The angle by which to increment the phase offset, in deg
        :param nodes: one or more nodes to which to apply the phase cycling
        :return: nothing
        TODO: What to do if an additional phase offset is required, e.g., for spin-echo sequences with flips about the
        y-axis? Maybe introduce a second phase variable to TXAtoms/ADCAtoms and compute the total phase as the sum of
        the phases?
        """

        if isinstance(idx, str):  # turn idx into a list of strings, even if it is only one
            idx = [idx]

        # append '_current' to each index:
        idx = list(map(lambda x: x + '_current', idx))

        for n in nodes:
            fun = lambda x: 1/2 * increment * (sum(x)**2 + sum(x) + 2)  # total phase for the current iteration. We sum
            # over all indices in the list idx
            pf = ParameterizedFunction(n, fun, idx)
            n.register_dynamic_properties([('dynamic_phase', pf)])

    def spoiler_gradient_node(self, voxel_size: float, axis: AtomType, multiple_pis: float = 5, max_ampl: float = 0, sr: float = 0) -> ActionNode:
        """
        Calculate a spoild gradient node along the specified (logical!) axis.
        :param voxel_size: The size of a voxel along the spoiling direction, in m
        :param axis: The axis along which to spoil. Has to be logical (AtomType.GR/GP/GS), not GX/GY/GZ!
        :param multiple_pis: The dispersion across a voxel, in multiples of pi.
        :param max_ampl: The maximum gradient amplitude (in mT/m). If unspecified (or 0), the maximum amplitude of the gradient system is used
        :param sr: The slew rate (in mT/m/ms). If unspecified or 0, the maximum slewrate of the gradient system is used.
        :return: An action node encapsulating the spoiler gradient
        """

        moment = multiple_pis*pi / (gamma_mT_µs*voxel_size)

        timing = self.grad_calc.timing_from_moment(moment)  # timing is (amplitude, duration, ramp-time)

        if axis == AtomType.GX or axis == AtomType.GY or axis == AtomType.GZ:
            raise ValueError('Error: SequenceBuilder.spoiler_gradient_node: axis has to be GR/GP/GS, not GX/GY/GZ.')

        atom = GradientAtom(0, 'Spoiler', timing[0], timing[1], timing[2], timing[2], axis, self.mediator.get_value('sequence_timing'))
        node = ActionNode('Spoiler_node', self.mediator, atom)
        return node

    @staticmethod
    def copy_tree(start_node: SequenceNode, prefix: str) -> SequenceNode:
        """
        Copy a (sub)tree, starting at start_node. All nodes are copies of the original nodes. Prefix is a string that is
        used to prefix all loop indices in the new tree to avoid confusion about shared indices between the original and
        new tree
        :param start_node: the root node of the tree to be copied
        :param prefix: a string that is used to prefix all loop indices found in the tree
        :return: the root node (i.e., the equivalent of start_node) of the copied tree
        """

        # the mediator object has to be the same for both trees, so we do not copy it
        memo = {id(start_node.mediator): start_node.mediator}
        root = deepcopy(start_node, memo)

        # find loop indices within new tree:
        indices_to_rename = root.get_loop_indices()
        root.rename_loop_indices(prefix, indices_to_rename)
        return root


if __name__ == '__main__':
    sb = SequenceBuilder()

    med = sb.mediator
    my_fov = 0.256
    nro = 10

    ro_comp = sb.readout_node('Readout_comp', my_fov, nro, 500, 100)

