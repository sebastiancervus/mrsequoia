"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from SequenceTiming import SequenceTiming


class SequenceMediator:
    """
    SequenceMediator manages a dictionary of sequence parameters, which can change during the execution of a sequence
    (such as loop indices, or the current time within the simulated sequence). The keys of the dictionary are strings
    identifying the variable through a meaningful name. The values of the dictionary are tuples of
    the form (id, value), where id is the id of the owner of that key (i.e., the object that initialized the key). and
    value is the value for the saved variable. Only the owner may change the value of the variable via the set_or_update
    method. This mechanism serves to avoid conflicts where two or more objects try to concurrently change a variable,
    thus leading to race conditions.
    Any object can query any variable via the get_value method.

    For cases where two or more objects need to change the value of a variable, there is a mechanism for handling such
    shared information. A variable can be initialized as shared using the initialize_shared_variable method. In that
    case, the SequenceMediator instance becomes the owner of the variable, and any object can change the value using
    the update_shared_variable method, In this case, the ownership check is skipped.
    """
    # the values in variables are a tuple with the form (id, value), where id is the id of the owner object (i.e.,
    # the object that initially registered that key. Only the possessor may change the variable, to avoid different
    # objects from modifying the same value

    def __init__(self):
        self.variables = {
            't': (id(self), 0),
            'sequence_timing': (id(self), SequenceTiming()),
            'simu_attr': (id(self), dict())
        }

    def set_or_update(self, requester: object, name: str, value) -> None:
        if name in self.variables:
            requester_id = id(requester)
            if self.variables[name][0] == requester_id:  # check if the requesting object owns the key
                tup = (requester_id, value)
                self.variables[name] = tup
            else:
                raise KeyError(f'Object with id {requester_id} is not the owner of the key {name}.')
        else:  # key does not yet exist in dict, create it and register the requester as the owner
            self.variables[name] = (id(requester), value)

    def get_value(self, name: str):
        return self.variables[name][1]

    def set_or_update_shared_variable(self, name: str, value=0) -> None:
        self.variables[name] = (id(self), value)

    def get_shared_variable(self, name: str):
        if name in self.variables:
            return self.variables[name][1]
        else:
            raise KeyError(f'No shared variable with name {name} found.')

    def set_simulation_attribute(self, attr: str, val: object) -> None:
        """
        Set an attribute for the simulation configuration. Since simu_attr is owned by the SequenceMediator, but values
        can be added by any object, we have to circumvent the id-checking mechanism of set_or_update and use this
        function instead. Calling this function will override any previous values for attr. This method can also be
        used to override automatically set values with custom ones.
        :param attr: the name of the attribute, a string
        :param val: the value to set (any type, but should produce something useful when str() is called on it
        :return: nothing.
        """
        self.variables['simu_attr'][1][attr] = val

    def increment_time(self, delta: float) -> None:
        """
        Increment the time by delta
        :param delta: the amount by which to increment (in µs)
        :return: Nothing
        """
        t = self.get_time()
        t += delta
        self.set_or_update(self, 't', t)

    def get_time(self) -> float:
        """
        Query the current time within the sequence
        :return: the time within the sequence
        """
        return self.variables['t'][1]

    def clear_timing(self) -> None:
        """
        Reset the sequence timing to the initial state
        :return: Nothing
        """
        self.variables['t'] = (id(self), 0)
        self.variables['sequence_timing'] = (id(self), SequenceTiming())