"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from SeqTree.SequenceNode import SequenceNode
from SeqTree.SequenceMediator import SequenceMediator
from typing import List, Tuple
from Atoms.AtomBase import AtomBase
from Atoms.GradientAtom import GradientAtom
from Atoms.TXAtom import TXAtom
from Atoms.ADCAtom import ADCAtom
from Atoms.FreqPhaseAtom import FreqPhaseAtom
from Atoms.SyncAtom import SyncAtom
from Atoms.AtomType import AtomType
from SequenceTiming import SequenceTiming
from copy import deepcopy
from SeqTree.ParameterizedFunction import ParameterizedFunction

DynamicAttributesListType = List[Tuple[str, ParameterizedFunction]]


class ActionNode(SequenceNode):

    def __init__(self, name: str, mediator: SequenceMediator, atom: AtomBase):
        super().__init__(name, mediator)
        self.atom: AtomBase = atom
        self.dynamic_attributes: DynamicAttributesListType = []
        self.call_counter_name = None

    def set_call_counter_name(self, call_counter_name):
        self.call_counter_name = call_counter_name
        if self.call_counter_name:
            self.mediator.set_or_update_shared_variable(call_counter_name)

    def execute(self) -> None:
        current_time = self.mediator.get_time()

        # We make a deep copy of the atom here before inserting it into the sequence timing list, since in the case of a
        # loop there will be multiple copies of the atom, with different start times and possibly other different
        # parameters. Just inserting a reference would mess up these differences.
        atom = deepcopy(self.atom)
        atom.set_start_time(current_time)
        self.update_dynamic_properties(atom)

        self.add_to_list(atom)

        if self.call_counter_name:
            curr_counter_val = self.mediator.get_value(self.call_counter_name)  # current counter value
            self.mediator.set_or_update_shared_variable(self.call_counter_name, curr_counter_val+1)

        self.run_children()

    @property
    def duration(self) -> float:
        """
        Calculate the duration of this atom, including all children. If there are not children, the duration is equal
        to the duration of the atom encapsulated in this node. If there are children, the duration is equal max(
        duration of atom, cumulative duration of all children)
        :return: The duration of this node including all children, in µs
        """
        if self.children is None or len(self.children) == 0:
            if isinstance(self.atom, GradientAtom):
                return self.atom.total_time
            else:
                return self.atom.duration
        else:
            child_dur = self.get_duration_of_children()
            own_duration = self.atom.total_time if isinstance(self.atom, GradientAtom) else self.atom.duration
            return max(own_duration, child_dur)

    def add_to_list(self, atom: AtomBase):

        st: SequenceTiming = self.mediator.get_value('sequence_timing')

        if isinstance(atom, GradientAtom):
            if atom.axis == AtomType.GR:
                st.gro += [atom]
            elif atom.axis == AtomType.GP:
                st.gpe += [atom]
            elif atom.axis == AtomType.GS:
                st.gss += [atom]
            else:
                raise ValueError('Invalid gradient axis: ' + str(self.atom.axis))

        elif isinstance(atom, TXAtom):
            st.tx += [atom]
        elif isinstance(atom, FreqPhaseAtom):
            st.fre += [atom]
        elif isinstance(atom, SyncAtom):
            st.syn += [atom]
        elif isinstance(atom, ADCAtom):
            st.adc += [atom]
        else:
            raise ValueError('Invalid atom type: ' + str(type(atom)))

    def get_attributes_as_string(self) -> str:
        atom = self.atom
        if isinstance(atom, TXAtom):
            #return f'TXAtom, angle={atom.angle}, duration={atom.duration}'
            return 'blah'
        elif isinstance(atom, GradientAtom):
            direction = atom.get_axis_as_string()
            #return f'GradientAtom, axis={direction}, ampl={atom.amplitude}, rut={atom.rut}'
            return 'blah'
        else:
            #return f'{atom.duration}'
            return 'blah'

    def register_dynamic_properties(self, attribs: DynamicAttributesListType) -> None:
        """
        Register the dynamic attributes of the encapsulated Atom object that are to be re-calculated for each iteration
        :param attribs:
        :return:
        """
        self.dynamic_attributes += attribs

    def update_dynamic_properties(self, atom: AtomBase) -> None:

        for t in self.dynamic_attributes:
            prop, fun = t
            val = fun.get_value()
            setattr(atom, prop, val)

    def get_additional_info(self) -> str:
        if isinstance(self.atom, GradientAtom):
            return f'amplitude: {self.atom.amplitude:.2f} mT/m,<BR/>duration: {self.atom.duration:.2f} µs,<BR/>total time: {self.atom.duration+self.atom.rdt:.2f} µs,<BR/>rut: {self.atom.rut:.2f} µs, rdt: {self.atom.rdt:.2f} µs'
        elif isinstance(self.atom, TXAtom):
            return f'flip angle: {self.atom.angle:.2f}°,<BR/>duration: {self.atom.duration:.2f} µs'
        elif isinstance(self.atom, ADCAtom):
            return f'sample points: {self.atom.num_adcs},<BR/>duration: {self.duration:.2f} µs'

        return 'TBD'

    def get_style_and_shape(self) -> str:

        if isinstance(self.atom, GradientAtom):
            if self.atom.axis == AtomType.GS:
                fill = f'{self.gs_color}:{self.gradient_base_color}'
            elif self.atom.axis == AtomType.GP:
                fill = f'{self.gp_color}:{self.gradient_base_color}'
            elif self.atom.axis == AtomType.GR:
                fill = f'{self.gr_color}:{self.gradient_base_color}'
            else:
                fill = 'white'
            return f'shape=trapezium, fillcolor="{fill}", style=radial, margin="0,0"'
        elif isinstance(self.atom, TXAtom):
            return f'shape=diamond, fillcolor="{self.rf_color}", style=filled'
        elif isinstance(self.atom, ADCAtom):
            return f'shape=egg, fillcolor="{self.adc_color}", style=filled, margin="0,0"'
        return ''

    def rename_loop_indices(self, prefix: str, indices_to_rename=None) -> None:
        """
        Rename the loop indices of all children by appending prefix + "_" to them. This is used when cloning a tree, e.g.,
        to create prepscans.
        :param prefix: a string that will be prepended to each loop index found.
        :param indices_to_rename: a list of all indices found so far. This will be used by all ActionNode instances
        that have to rename their dependencies on dynamic variables.
        :return: nothing
        """

        if indices_to_rename is None:
            indices_to_rename = []
        for da in self.dynamic_attributes:
            pf = da[1]  # extract the ParameterizedFunction object
            for ir in indices_to_rename:
                pf.prefix_params(ir, prefix)
                pf.prefix_params(ir + '_runs', prefix)
                pf.prefix_params(ir + '_current', prefix)

        for c in self.children:
            c.rename_loop_indices(prefix, indices_to_rename)



