"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from typing import Callable, List

SeqFun = Callable[[List[float]], float]
ParamList = List[str]


class ParameterizedFunction:
    """
    A class that encapsulates a function to calculate parameters of pulses depending on looping variables and
    possibly member variables. Each ParameterizedFunction object is associated with its owner (of type ActionNode)
    The function is a Callable (i.e. a lambda or reference to a proper function) with a single argument x. x is a
    list of any number of float values, so the function should be defined in terms of x[0], x[1], etc. At each
    execution of an ActionNode, the values of x are updated with the values indicated by ParamList, a list of
    strings. For each string, the following procedure is applied to resolve the value:
    1. If the owner's atom has an attribute of this name, use its value.
    2. If the owner itself has an attribute of this name, use its value.
    3. Query the SequenceMediator object for the string.
    Options 1 and 2 also work with methods that are declared using the @property decorator!
    If neither resolves to a numerical value, an error is raised.
    """

    def __init__(self, owner: 'ActionNode', fun: SeqFun, params: ParamList):
        super().__init__()
        self.owner: 'ActionNode' = owner
        self.fun: SeqFun = fun
        self.params: List[str] = params

    def get_value(self) -> float:

        pvals = []
        for p in self.params:
            pvals += [self.__resolve_param(p)]

        return self.fun(pvals)

    def __resolve_param(self, p: str) -> float:
        val = None
        if hasattr(self.owner.atom, p):
            val = getattr(self.owner.atom, p)
        elif hasattr(self.owner, p):
            val = getattr(self.owner, p)
        else:
            val = self.owner.mediator.get_value(p)  # this will raise an uncaught KeyError if p does not exist in the
            # mediator

        return val

    def prefix_params(self, old_param: str, prefix: str) -> None:
        """
        Add  prefix + "_" to old_param, if old_param is in the list of parameters for this function. This is used to
        update the dependence on loop parameters if the loop indices are renamed, e.g. when copying a part of a
        SeqTree.
        :param old_param: the parameter to be renamed (str)
        :param prefix: the prefix to prepend to old_param (plus an underscore)
        :return: nothing
        """
        for i in range(0, len(self.params)):
            if self.params[i] == old_param:
                self.params[i] = prefix + '_' + self.params[i]
