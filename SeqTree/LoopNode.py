"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from SeqTree.SequenceNode import SequenceNode
from SeqTree.SequenceMediator import SequenceMediator
from typing import List
from SeqTree.DelayNode import DelayNode


class LoopNode(SequenceNode):

    def __init__(self, name: str, mediator: SequenceMediator, idx_id: str, n_runs: int = 1, duration: float = 0):
        super().__init__(name, mediator)
        self._number_of_runs: int = n_runs

        self._idx_id: str = idx_id  # The node will register two properties with SequenceMediator. One is _idx_id + '_runs'
        # for the total number of runs, the other one is _idx_id + '_current' with the current id of the current run.
        self._idx_id_current: str = idx_id + '_current'
        self._idx_id_runs: str = idx_id + '_runs'
        self.mediator.set_or_update(self, self._idx_id_runs, n_runs)
        self.mediator.set_or_update(self, self._idx_id_current, 0)
        self._run_idx: int = 0
        self._duration: float = duration

    def execute(self) -> None:
        for it in range(0, self._number_of_runs):
            self._run_idx = it
            self.mediator.set_or_update(self, self._idx_id_current, self._run_idx)
            self.run_children()

    def run(self) -> None:
        self.execute()

    def set_number_of_runs(self, n_runs) -> None:
        """
        Set the number of runs, and broadcast it to the mediator
        :param n_runs: the number of runs of this loop
        :return: nothing
        """
        self._number_of_runs = n_runs
        self.mediator.set_or_update(self, self._idx_id_runs, n_runs)

    @property
    def duration(self) -> float:
        """
        Get the duration of all runs of this loop
        :return: The total duration of this loop, including all iterations.
        """
        if self.children is None or len(self.children) == 0:
            return 0
        else:
            return self.get_duration_of_children() * self._number_of_runs

    @property
    def loop_idx_str(self) -> str:
        return self._idx_id

    def run_children(self) -> None:
        for c in self.children:
            c.run()

    def get_additional_info(self) -> str:
        return f'index: {self._idx_id},<BR/> runs: {self._number_of_runs},<BR/>duration: {self.get_duration_of_children()} µs'

    def get_style_and_shape(self) -> str:
        return f'shape=doubleoctagon, fillcolor="{self.loop_color}", style=filled'

    def insert_filltime(self) -> None:
        """
        Insert delay at the end of the loop, if necessary, to achieve the duration specified in the _duration property
        :return: nothing
        """
        # delete any trailing delay, if present:
        while len(self.children) > 0 and isinstance(self.children[-1], DelayNode):
            self.children = self.children[:-1]  # delete last node

        super().insert_filltime()

        if self._duration > 0:
            child_duration = self.get_duration_of_children()
            if child_duration < self._duration:
                children = list(self.children)
                children += [DelayNode("Delay", self.mediator, self._duration - child_duration)]
                self.children = children
            else:
                print(f'The duration of the children of {self.name} is larger than the specified duration: {child_duration} µs vs. {self._duration} µs')

    def get_loop_indices(self, indices=None) -> List[str]:
        if indices is None:
            indices = []
        indices += [self._idx_id]
        indices = super().get_loop_indices(indices)  # call child nodes
        return indices

    def rename_loop_indices(self, prefix: str, indices_to_rename=None) -> None:
        """
        Rename the loop indices of all children by appending prefix + "_" to them. This is used when cloning a tree, e.g.,
        to create prepscans.
        :param prefix: a string that will be prepended to each loop index found.
        :param indices_to_rename: a list of all indices found so far. This will be used by all ActionNode instances
        that have to rename their dependencies on dynamic variables.
        :return: nothing
        """

        if indices_to_rename is None:
            indices_to_rename = []
        indices_to_rename += [self._idx_id]
        self._idx_id = prefix + '_' + self._idx_id
        self._idx_id_runs = prefix + '_' + self._idx_id_runs
        self._idx_id_current = prefix + '_' + self._idx_id_current
        #  register the new indices with the mediator:
        self.mediator.set_or_update(self, self._idx_id_runs, self._number_of_runs)
        self.mediator.set_or_update(self, self._idx_id_current, 0)

        for c in self.children:
            c.rename_loop_indices(prefix, indices_to_rename)

    @staticmethod
    def tr_loop(mediator: SequenceMediator, n_runs: int, duration: float = 0) -> 'LoopNode':
        """
        Generate a generic TR loop
        :param mediator:
        :param n_runs: number of executions
        :param duration: the desired duration, in µs, If omitted (or <= 0), the minimum duration without any fill times
        will be used
        :return: The LoopNode object
        """
        return LoopNode('TR-loop', mediator, 'tr_idx', n_runs, duration)

    @staticmethod
    def pe_loop(mediator: SequenceMediator, n_runs: int, duration: float = 0) -> 'LoopNode':
        """
        Generate a generic phase-encode loop
        :param mediator:
        :param n_runs: number of executions
        :param duration: the desired duration, in µs, If omitted (or <= 0), the minimum duration without any fill times
        will be used
        :return: The LoopNode object
        """
        return LoopNode('PE-loop', mediator, 'pe_idx', n_runs, duration)


