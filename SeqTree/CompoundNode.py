"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from SeqTree.SequenceNode import SequenceNode
from SeqTree.SequenceMediator import SequenceMediator


class CompoundNode(SequenceNode):
    """
    A node that does nothing in particular, except execute its children. Can be used to group events together,
    e.g. bipolar trapezoidal gradients
    """
    def __init__(self, name: str, mediator: SequenceMediator):
        super().__init__(name, mediator)

    def execute(self) -> None:
        self.run_children()

    @property
    def duration(self) -> float:
        return self.get_duration_of_children()

    def get_style_and_shape(self) -> str:
        return f'shape=invtriangle, fillcolor="{self.compound_color}", style=filled'

    def get_additional_info(self) -> str:
        return f'duration: {self.get_duration_of_children():.2f} µs'
