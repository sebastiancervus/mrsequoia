"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""

from SeqTree.ActionNode import ActionNode
from SeqTree.SequenceNode import SequenceNode
from SeqTree.SequenceMediator import SequenceMediator


class DelayNode(ActionNode):

    def __init__(self, name: str, mediator: SequenceMediator, duration: int):
        super().__init__(name, mediator, atom=None)
        self.__duration: int = duration

    def execute(self) -> None:
        pass

    @property
    def duration(self) -> float:
        """
        Get the duration of this node. Since a delay node should not have any children, the duration is just the
        duration of the delay
        :return: The duration of this node
        """
        return self.__duration

    def get_additional_info(self) -> str:
        return f'duration: {self.__duration:.2f} µs'

    def get_style_and_shape(self) -> str:
        return f'shape=cds, fillcolor="{self.delay_color}", style=filled, margin="0.2,0.2"'
