#!/bin/bash

rm -rf htmlcov

poetry run coverage run --branch -m pytest
if [ $? -eq 0 ]; then
	poetry run coverage report -m
else
	ret=$?
	echo "An error ocurred."
	exit $ret
fi
if [ $? -eq 0 ]; then
	poetry run coverage html
else
	ret=$?
	echo "An error ocurred."
	exit $ret
fi
