#!/bin/bash

target_base_dir="tmp"
target_dir="$target_base_dir/MRSequoia"
zipfile="MRSequoia.zip"
curr_dir=$(pwd)
files="*.py LICENSE.txt *.md *.pdf Pipfile Pipfile.lock JupyterLab_example.ipynb build_docker.sh run_docker.sh Dockerfile examples/FLASH/*"

if [ -f $zipfile ]; then
	echo "Deleting $zipfile"
	rm $zipfile
fi

if [ -d $target_dir ]; then
	echo "Deleting $target_dir"
	rm -rf $target_dir
fi

bash pandoc.sh

mkdir -p $target_dir

#cp -r $files $target_dir
for x in $files; do
	rsync -avR $x $target_dir  # -R is necessary to preserve the full path, so that examples/FLASH gets copied to MRSequoia/examples/FLASH and not to examples/FLASH
done

cd $target_base_dir

zip -r "$curr_dir/MRSequoia.zip" MRSequoia

cd $curr_dir

rm -rf $target_base_dir
