"""
Written by Sebastian Hirsch.
sebastian.hirsch@charite.de
See LICENSE.txt for licensing information.
"""


class SimulationProtocol:

    def __init__(self, *args):
        raise NotImplementedError('''This is the generic base class for the simulation protocol. So far, only support for Siemens simulation
                    files exist. Please download the MRSequoia_SiemensTools package from https://www.magnetom.net/t/introducing-mrsequoia/3682/5 and install according to the
                    manual''')

    def __getitem__(self, item: str):
        raise NotImplementedError('''This is the generic base class for the simulation protocol. So far, only support for Siemens simulation
                    files exist. Please download the MRSequoia_SiemensTools package from https://www.magnetom.net/t/introducing-mrsequoia/3682/5 and install according to the
                    manual''')

    def generate_simu_dict(self) -> dict:
        raise NotImplementedError('''This is the generic base class for the simulation protocol. So far, only support for Siemens simulation
            files exist. Please download the MRSequoia_SiemensTools package from https://www.magnetom.net/t/introducing-mrsequoia/3682/5 and install according to the
            manual''')
