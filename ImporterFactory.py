from typing import TypeVar, Optional

from Importer import Importer
from SiemensTools.SiemensINFImporter import SiemensINFImporter

SeqTim = TypeVar('SeqTim', bound='SequenceTiming')


class ImporterFactory:
    """
    A class to serve as a factory for constructing the right type of vendor-specific importer, based on the name of
    the input file.
    """

    @staticmethod
    def make_importer(filename: str, timing: SeqTim) -> Optional[Importer]:

        if filename[-4:].lower() == '.dsv':
            return SiemensINFImporter(filename, timing)
        else:
            print("Don't know what to do with file " + filename)
            return None
